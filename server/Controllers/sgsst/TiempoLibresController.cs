using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/TiempoLibres")]
  [Route("mvc/odata/sgsst/TiempoLibres")]
  public partial class TiempoLibresController : ODataController
  {
    private Data.SgsstContext context;

    public TiempoLibresController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/TiempoLibres
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.TiempoLibre> GetTiempoLibres()
    {
      var items = this.context.TiempoLibres.AsQueryable<Models.Sgsst.TiempoLibre>();
      this.OnTiempoLibresRead(ref items);

      return items;
    }

    partial void OnTiempoLibresRead(ref IQueryable<Models.Sgsst.TiempoLibre> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<TiempoLibre> GetTiempoLibre(Int64 key)
    {
        var items = this.context.TiempoLibres.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnTiempoLibreDeleted(Models.Sgsst.TiempoLibre item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteTiempoLibre(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.TiempoLibres
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.TiempoLibre>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnTiempoLibreDeleted(item);
            this.context.TiempoLibres.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnTiempoLibreUpdated(Models.Sgsst.TiempoLibre item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutTiempoLibre(Int64 key, [FromBody]Models.Sgsst.TiempoLibre newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TiempoLibres
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.TiempoLibre>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnTiempoLibreUpdated(newItem);
            this.context.TiempoLibres.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.TiempoLibres.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchTiempoLibre(Int64 key, [FromBody]Delta<Models.Sgsst.TiempoLibre> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TiempoLibres.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.TiempoLibre>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnTiempoLibreUpdated(item);
            this.context.TiempoLibres.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.TiempoLibres.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnTiempoLibreCreated(Models.Sgsst.TiempoLibre item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.TiempoLibre item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnTiempoLibreCreated(item);
            this.context.TiempoLibres.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/TiempoLibres/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
