import { Component, Injector } from '@angular/core';
import { EditEstadoCivilGenerated } from './edit-estado-civil-generated.component';

@Component({
  selector: 'page-edit-estado-civil',
  templateUrl: './edit-estado-civil.component.html'
})
export class EditEstadoCivilComponent extends EditEstadoCivilGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
