using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/ClaseRiesgos")]
  [Route("mvc/odata/sgsst/ClaseRiesgos")]
  public partial class ClaseRiesgosController : ODataController
  {
    private Data.SgsstContext context;

    public ClaseRiesgosController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/ClaseRiesgos
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.ClaseRiesgo> GetClaseRiesgos()
    {
      var items = this.context.ClaseRiesgos.AsQueryable<Models.Sgsst.ClaseRiesgo>();
      this.OnClaseRiesgosRead(ref items);

      return items;
    }

    partial void OnClaseRiesgosRead(ref IQueryable<Models.Sgsst.ClaseRiesgo> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<ClaseRiesgo> GetClaseRiesgo(Int64 key)
    {
        var items = this.context.ClaseRiesgos.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnClaseRiesgoDeleted(Models.Sgsst.ClaseRiesgo item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteClaseRiesgo(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.ClaseRiesgos
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .Include(i => i.EmpresaClaseRisgos)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.ClaseRiesgo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnClaseRiesgoDeleted(item);
            this.context.ClaseRiesgos.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnClaseRiesgoUpdated(Models.Sgsst.ClaseRiesgo item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutClaseRiesgo(Int64 key, [FromBody]Models.Sgsst.ClaseRiesgo newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ClaseRiesgos
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .Include(i => i.EmpresaClaseRisgos)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.ClaseRiesgo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnClaseRiesgoUpdated(newItem);
            this.context.ClaseRiesgos.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.ClaseRiesgos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchClaseRiesgo(Int64 key, [FromBody]Delta<Models.Sgsst.ClaseRiesgo> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ClaseRiesgos.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.ClaseRiesgo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnClaseRiesgoUpdated(item);
            this.context.ClaseRiesgos.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.ClaseRiesgos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnClaseRiesgoCreated(Models.Sgsst.ClaseRiesgo item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.ClaseRiesgo item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnClaseRiesgoCreated(item);
            this.context.ClaseRiesgos.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/ClaseRiesgos/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
