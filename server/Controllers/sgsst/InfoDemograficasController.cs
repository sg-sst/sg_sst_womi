using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/InfoDemograficas")]
  [Route("mvc/odata/sgsst/InfoDemograficas")]
  public partial class InfoDemograficasController : ODataController
  {
    private Data.SgsstContext context;

    public InfoDemograficasController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/InfoDemograficas
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.InfoDemografica> GetInfoDemograficas()
    {
      var items = this.context.InfoDemograficas.AsQueryable<Models.Sgsst.InfoDemografica>();
      this.OnInfoDemograficasRead(ref items);

      return items;
    }

    partial void OnInfoDemograficasRead(ref IQueryable<Models.Sgsst.InfoDemografica> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<InfoDemografica> GetInfoDemografica(Int64 key)
    {
        var items = this.context.InfoDemograficas.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnInfoDemograficaDeleted(Models.Sgsst.InfoDemografica item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteInfoDemografica(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.InfoDemograficas
                .Where(i => i.Id == key)
                .Include(i => i.EnfermedadesInfoDemofraficas)
                .Include(i => i.ActividadesPersonas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.InfoDemografica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnInfoDemograficaDeleted(item);
            this.context.InfoDemograficas.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnInfoDemograficaUpdated(Models.Sgsst.InfoDemografica item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutInfoDemografica(Int64 key, [FromBody]Models.Sgsst.InfoDemografica newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.InfoDemograficas
                .Where(i => i.Id == key)
                .Include(i => i.EnfermedadesInfoDemofraficas)
                .Include(i => i.ActividadesPersonas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.InfoDemografica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnInfoDemograficaUpdated(newItem);
            this.context.InfoDemograficas.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.InfoDemograficas.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "Persona,ClaseRiesgo,EstadoCivil,TiempoLibre,PromedioIngreso,RangoIngreso,TipoContrato,IntensidadDeporte");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchInfoDemografica(Int64 key, [FromBody]Delta<Models.Sgsst.InfoDemografica> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.InfoDemograficas.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.InfoDemografica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnInfoDemograficaUpdated(item);
            this.context.InfoDemograficas.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.InfoDemograficas.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "Persona,ClaseRiesgo,EstadoCivil,TiempoLibre,PromedioIngreso,RangoIngreso,TipoContrato,IntensidadDeporte");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnInfoDemograficaCreated(Models.Sgsst.InfoDemografica item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.InfoDemografica item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnInfoDemograficaCreated(item);
            this.context.InfoDemograficas.Add(item);
            this.context.SaveChanges();

            var key = item.Id;

            var itemToReturn = this.context.InfoDemograficas.Where(i => i.Id == key);

            Request.QueryString = Request.QueryString.Add("$expand", "Persona,ClaseRiesgo,EstadoCivil,TiempoLibre,PromedioIngreso,RangoIngreso,TipoContrato,IntensidadDeporte");

            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
