import { Component, Injector } from '@angular/core';
import { EditPromedioIngresoGenerated } from './edit-promedio-ingreso-generated.component';

@Component({
  selector: 'page-edit-promedio-ingreso',
  templateUrl: './edit-promedio-ingreso.component.html'
})
export class EditPromedioIngresoComponent extends EditPromedioIngresoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
