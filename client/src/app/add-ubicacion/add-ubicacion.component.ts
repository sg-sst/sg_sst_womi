import { Component, Injector } from '@angular/core';
import { AddUbicacionGenerated } from './add-ubicacion-generated.component';

@Component({
  selector: 'page-add-ubicacion',
  templateUrl: './add-ubicacion.component.html'
})
export class AddUbicacionComponent extends AddUbicacionGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
