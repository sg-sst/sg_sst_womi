import { Component, Injector } from '@angular/core';
import { AddInfoDemograficaGenerated } from './add-info-demografica-generated.component';

@Component({
  selector: 'page-add-info-demografica',
  templateUrl: './add-info-demografica.component.html'
})
export class AddInfoDemograficaComponent extends AddInfoDemograficaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
