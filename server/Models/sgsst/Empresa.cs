using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Empresa", Schema = "public")]
  public partial class Empresa
  {
    [ConcurrencyCheck]
    public string Nit
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string RazonSocial
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public Int64 Actividad_Economica_Id
    {
      get;
      set;
    }

    [ForeignKey("Actividad_Economica_Id")]
    public ActividadEconomica ActividadEconomica { get; set; }
    [ConcurrencyCheck]
    public Int64 Ubicacion_Id
    {
      get;
      set;
    }

    [ForeignKey("Ubicacion_Id")]
    public Ubicacion Ubicacion { get; set; }
    [ConcurrencyCheck]
    public string Direccion
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Correo
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Telefono
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Fax
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Representante_Nombre
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Representante_Documento
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public decimal? Prima_Cotizacion_Mensual
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public DateTime FechaCrea
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public DateTime? FechaMod
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public Int64? Estado_Empresa_Id
    {
      get;
      set;
    }

    [ForeignKey("Estado_Empresa_Id")]
    public EstadoEmpresa EstadoEmpresa { get; set; }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("Empresa")]
    public ICollection<Documento> Documentos { get; set; }

    [InverseProperty("Empresa")]
    public ICollection<Persona> Personas { get; set; }

    [InverseProperty("Empresa")]
    public ICollection<EmpresaClaseRisgo> EmpresaClaseRisgos { get; set; }
  }
}
