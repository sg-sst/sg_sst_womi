using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/EstadoCivils")]
  [Route("mvc/odata/sgsst/EstadoCivils")]
  public partial class EstadoCivilsController : ODataController
  {
    private Data.SgsstContext context;

    public EstadoCivilsController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/EstadoCivils
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.EstadoCivil> GetEstadoCivils()
    {
      var items = this.context.EstadoCivils.AsQueryable<Models.Sgsst.EstadoCivil>();
      this.OnEstadoCivilsRead(ref items);

      return items;
    }

    partial void OnEstadoCivilsRead(ref IQueryable<Models.Sgsst.EstadoCivil> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<EstadoCivil> GetEstadoCivil(Int64 key)
    {
        var items = this.context.EstadoCivils.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnEstadoCivilDeleted(Models.Sgsst.EstadoCivil item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteEstadoCivil(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.EstadoCivils
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.EstadoCivil>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEstadoCivilDeleted(item);
            this.context.EstadoCivils.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEstadoCivilUpdated(Models.Sgsst.EstadoCivil item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutEstadoCivil(Int64 key, [FromBody]Models.Sgsst.EstadoCivil newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.EstadoCivils
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.EstadoCivil>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEstadoCivilUpdated(newItem);
            this.context.EstadoCivils.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.EstadoCivils.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchEstadoCivil(Int64 key, [FromBody]Delta<Models.Sgsst.EstadoCivil> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.EstadoCivils.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.EstadoCivil>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnEstadoCivilUpdated(item);
            this.context.EstadoCivils.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.EstadoCivils.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEstadoCivilCreated(Models.Sgsst.EstadoCivil item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.EstadoCivil item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnEstadoCivilCreated(item);
            this.context.EstadoCivils.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/EstadoCivils/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
