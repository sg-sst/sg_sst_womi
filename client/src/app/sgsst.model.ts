export interface ActividadEconomica {
  Nombre: string;
  Id: number;
}

export interface ActividadesBienestar {
  Nombre: string;
  Id: number;
}

export interface ActividadesPersona {
  Actividad_Bienestar_Id: number;
  Info_Demografica_Id: number;
}

export interface ClaseRiesgo {
  Nombre: string;
  Id: number;
}

export interface Documento {
  Nombre: string;
  Tipo_Archivo_Id: number;
  Empresa_Id: number;
  Ruta: string;
  Fecha_Mod: string;
  Id: number;
  Fecha_Crea: string;
}

export interface Empresa {
  Nit: string;
  RazonSocial: string;
  Actividad_Economica_Id: number;
  Ubicacion_Id: number;
  Direccion: string;
  Correo: string;
  Telefono: string;
  Fax: string;
  Representante_Nombre: string;
  Representante_Documento: string;
  Prima_Cotizacion_Mensual: number;
  FechaCrea: string;
  FechaMod: string;
  Estado_Empresa_Id: number;
  Id: number;
}

export interface EmpresaClaseRisgo {
  Empresa_Id: number;
  Clase_Riesgo_Id: number;
}

export interface Enfermedade {
  Nombre: string;
  Id: number;
}

export interface EnfermedadesInfoDemofrafica {
  Enfermedad_Id: number;
  Info_Demografica_Id: number;
}

export interface EstadoCivil {
  Nombre: string;
  Id: number;
}

export interface EstadoEmpresa {
  Nombre: string;
  Id: number;
}

export interface InfoDemografica {
  Persona_Id: number;
  Cargo: string;
  Fecha_Ini_Cargo: string;
  Clase_Riesgo_Id: number;
  Estado_Civil_Id: number;
  Tiene_Vivienda: boolean;
  Num_Persona: number;
  Tiempo_Libre_Id: number;
  Promedio_Ingresos_Id: number;
  Rango_Ingresos_Id: number;
  Tipo_Contrato_Id: number;
  Tiene_Enfermedad: boolean;
  Fuma: boolean;
  Promedio_Fuma: number;
  Practica_Deporte: boolean;
  Intensidad_Deporte_Id: number;
  Consentimiento: boolean;
  Id: number;
  Fecha_Crea: string;
}

export interface IntensidadDeporte {
  Nombre: string;
  Id: number;
}

export interface Persona {
  TipoDocumento: number;
  Documento: number;
  Nombre: string;
  Apellidos: string;
  Email: string;
  Telefono: string;
  Direccion: string;
  Fecha_Nac: string;
  Genero: string;
  Empresa_Id: number;
  Id: number;
  Fecha_Crea: string;
}

export interface PromedioIngreso {
  Nombre: string;
  Id: number;
}

export interface RangoIngreso {
  Nombre: string;
  Id: number;
}

export interface TiempoLibre {
  Nombre: string;
  Id: number;
}

export interface TipoArchivo {
  Nombre: string;
  Id: number;
}

export interface TipoContrato {
  Nombre: string;
  Id: number;
}

export interface Ubicacion {
  Nombre: number;
  Padre_Id: number;
  Id: number;
}
