using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Enfermedades", Schema = "public")]
  public partial class Enfermedade
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("Enfermedade")]
    public ICollection<EnfermedadesInfoDemofrafica> EnfermedadesInfoDemofraficas { get; set; }
  }
}
