import { Component, Injector } from '@angular/core';
import { EditRangoIngresoGenerated } from './edit-rango-ingreso-generated.component';

@Component({
  selector: 'page-edit-rango-ingreso',
  templateUrl: './edit-rango-ingreso.component.html'
})
export class EditRangoIngresoComponent extends EditRangoIngresoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
