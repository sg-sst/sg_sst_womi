using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Ubicacion", Schema = "public")]
  public partial class Ubicacion
  {
    [ConcurrencyCheck]
    public Int64 Nombre
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public Int64? Padre_Id
    {
      get;
      set;
    }

    [ForeignKey("Padre_Id")]
    public Ubicacion Ubicacion1 { get; set; }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("Ubicacion")]
    public ICollection<Empresa> Empresas { get; set; }

    [InverseProperty("Ubicacion1")]
    public ICollection<Ubicacion> Ubicacions1 { get; set; }
  }
}
