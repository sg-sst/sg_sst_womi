using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Builder;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.SpaServices.AngularCli;

using Sgsst.Data;
using Sgsst.Models;
using Sgsst.Authentication;

namespace Sgsst
{
  public partial class Startup
  {
    public Startup(IConfiguration configuration, IHostingEnvironment env)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    partial void OnConfigureServices(IServiceCollection services);

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddOptions();
      services.AddLogging(logging =>
      {
          logging.AddConsole();
          logging.AddDebug();
      });

      services.AddCors(options =>
      {
          options.AddPolicy(
              "AllowAny",
              x =>
              {
                  x.AllowAnyHeader()
                  .AllowAnyMethod()
                  .SetIsOriginAllowed(isOriginAllowed: _ => true)
                  .AllowCredentials();
              });
      });
      services.AddMvc(options =>
      {
          options.FormatterMappings.SetMediaTypeMappingForFormat("csv", "text/csv");
          options.OutputFormatters.Add(new Sgsst.Data.CsvDataContractSerializerOutputFormatter());
          options.OutputFormatters.Add(new Sgsst.Data.XlsDataContractSerializerOutputFormatter());
      });

      services.AddAuthorization();
      services.AddOData();
      services.AddODataQueryFilter();
      services.AddHttpContextAccessor();

      var tokenValidationParameters = new TokenValidationParameters
      {
          ValidateIssuerSigningKey = true,
          IssuerSigningKey = TokenProviderOptions.Key,
          ValidateIssuer = true,
          ValidIssuer = TokenProviderOptions.Issuer,
          ValidateAudience = true,
          ValidAudience = TokenProviderOptions.Audience,
          ValidateLifetime = true,
          ClockSkew = TimeSpan.Zero
      };

      services.AddAuthentication(options =>
      {
          options.DefaultScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
      }).AddJwtBearer(options =>
      {
          options.Audience = TokenProviderOptions.Audience;
          options.ClaimsIssuer = TokenProviderOptions.Issuer;
          options.TokenValidationParameters = tokenValidationParameters;
          options.SaveToken = true;
      });
      services.AddDbContext<ApplicationIdentityDbContext>(options =>
      {
          options.UseNpgsql(Configuration.GetConnectionString("sgsstConnection"));
      });

      services.AddIdentity<ApplicationUser, IdentityRole>()
              .AddEntityFrameworkStores<ApplicationIdentityDbContext>()
              .AddDefaultTokenProviders();

      services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, ApplicationPrincipalFactory>();


      services.AddDbContext<Sgsst.Data.SgsstContext>(options =>
      {
        options.UseNpgsql(Configuration.GetConnectionString("sgsstConnection"));
      });

      OnConfigureServices(services);
    }

    partial void OnConfigure(IApplicationBuilder app);
    partial void OnConfigure(IApplicationBuilder app, IHostingEnvironment env);
    partial void OnConfigureOData(ODataConventionModelBuilder builder);

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {

      IServiceProvider provider = app.ApplicationServices.GetRequiredService<IServiceProvider>();
      app.UseCors("AllowAny");
      app.Use(async (context, next) => {
          if (context.Request.Path.Value == "/__ssrsreport" || context.Request.Path.Value == "/ssrsproxy") {
            await next();
            return;
          }
          try
          {
              await next();
          }
#pragma warning disable 0168
          catch (Microsoft.AspNetCore.Mvc.Internal.AmbiguousActionException ex) {
#pragma warning restore 0168
              if (!Path.HasExtension(context.Request.Path.Value)) {
                  context.Request.Path = "/index.html";
                  await next();
              }
          }

          if ((context.Response.StatusCode == 404 || context.Response.StatusCode == 401) && !Path.HasExtension(context.Request.Path.Value)) {
              context.Request.Path = "/index.html";
              await next();
          }
      });

      app.UseDefaultFiles();
      app.UseStaticFiles();

      app.UseDeveloperExceptionPage();

      app.UseMvc(builder =>
      {
          builder.Count().Filter().OrderBy().Expand().Select().MaxTop(null).SetTimeZoneInfo(TimeZoneInfo.Utc);

          if (env.EnvironmentName == "Development")
          {
              builder.MapRoute(name: "default",
                template: "{controller}/{action}/{id?}",
                defaults: new { controller = "Home", action = "Index" }
              );
          }

          var oDataBuilder = new ODataConventionModelBuilder(provider);

          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.ActividadEconomica>("ActividadEconomicas");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.ActividadesBienestar>("ActividadesBienestars");

          var actividadesPersonaEntitySet = oDataBuilder.EntitySet<Sgsst.Models.Sgsst.ActividadesPersona>("ActividadesPersonas");
          actividadesPersonaEntitySet.EntityType.HasKey(entity => new {
            entity.Actividad_Bienestar_Id, entity.Info_Demografica_Id
          });
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.ClaseRiesgo>("ClaseRiesgos");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.Documento>("Documentos");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.Empresa>("Empresas");

          var empresaClaseRisgoEntitySet = oDataBuilder.EntitySet<Sgsst.Models.Sgsst.EmpresaClaseRisgo>("EmpresaClaseRisgos");
          empresaClaseRisgoEntitySet.EntityType.HasKey(entity => new {
            entity.Empresa_Id, entity.Clase_Riesgo_Id
          });
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.Enfermedade>("Enfermedades");

          var enfermedadesInfoDemofraficaEntitySet = oDataBuilder.EntitySet<Sgsst.Models.Sgsst.EnfermedadesInfoDemofrafica>("EnfermedadesInfoDemofraficas");
          enfermedadesInfoDemofraficaEntitySet.EntityType.HasKey(entity => new {
            entity.Enfermedad_Id, entity.Info_Demografica_Id
          });
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.EstadoCivil>("EstadoCivils");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.EstadoEmpresa>("EstadoEmpresas");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.InfoDemografica>("InfoDemograficas");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.IntensidadDeporte>("IntensidadDeportes");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.Persona>("Personas");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.PromedioIngreso>("PromedioIngresos");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.RangoIngreso>("RangoIngresos");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.TiempoLibre>("TiempoLibres");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.TipoArchivo>("TipoArchivos");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.TipoContrato>("TipoContratos");
          oDataBuilder.EntitySet<Sgsst.Models.Sgsst.Ubicacion>("Ubicacions");

          this.OnConfigureOData(oDataBuilder);

          oDataBuilder.EntitySet<ApplicationUser>("ApplicationUsers");
          var usersType = oDataBuilder.StructuralTypes.First(x => x.ClrType == typeof(ApplicationUser));
          usersType.AddCollectionProperty(typeof(ApplicationUser).GetProperty("RoleNames"));
          oDataBuilder.EntitySet<IdentityRole>("ApplicationRoles");

          var model = oDataBuilder.GetEdmModel();

          builder.MapODataServiceRoute("odata/sgsst", "odata/sgsst", model);

          builder.MapODataServiceRoute("auth", "auth", model);
      });

      if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("RADZEN")) && env.IsDevelopment())
      {
        app.UseSpa(spa =>
        {
          spa.Options.SourcePath = "../client";
          spa.UseAngularCliServer(npmScript: "start -- --port 8000 --open");
        });
      }

      OnConfigure(app);
      OnConfigure(app, env);
    }
  }
}
