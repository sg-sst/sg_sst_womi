using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Intensidad_Deporte", Schema = "public")]
  public partial class IntensidadDeporte
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("IntensidadDeporte")]
    public ICollection<InfoDemografica> InfoDemograficas { get; set; }
  }
}
