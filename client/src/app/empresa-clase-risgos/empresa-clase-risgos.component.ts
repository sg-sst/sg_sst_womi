import { Component, Injector } from '@angular/core';
import { EmpresaClaseRisgosGenerated } from './empresa-clase-risgos-generated.component';

@Component({
  selector: 'page-empresa-clase-risgos',
  templateUrl: './empresa-clase-risgos.component.html'
})
export class EmpresaClaseRisgosComponent extends EmpresaClaseRisgosGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
