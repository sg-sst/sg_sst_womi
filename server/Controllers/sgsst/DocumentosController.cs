using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/Documentos")]
  [Route("mvc/odata/sgsst/Documentos")]
  public partial class DocumentosController : ODataController
  {
    private Data.SgsstContext context;

    public DocumentosController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/Documentos
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.Documento> GetDocumentos()
    {
      var items = this.context.Documentos.AsQueryable<Models.Sgsst.Documento>();
      this.OnDocumentosRead(ref items);

      return items;
    }

    partial void OnDocumentosRead(ref IQueryable<Models.Sgsst.Documento> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<Documento> GetDocumento(Int64 key)
    {
        var items = this.context.Documentos.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnDocumentoDeleted(Models.Sgsst.Documento item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteDocumento(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.Documentos
                .Where(i => i.Id == key)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Documento>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnDocumentoDeleted(item);
            this.context.Documentos.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnDocumentoUpdated(Models.Sgsst.Documento item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutDocumento(Int64 key, [FromBody]Models.Sgsst.Documento newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Documentos
                .Where(i => i.Id == key)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Documento>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnDocumentoUpdated(newItem);
            this.context.Documentos.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.Documentos.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "TipoArchivo,Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchDocumento(Int64 key, [FromBody]Delta<Models.Sgsst.Documento> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Documentos.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.Documento>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnDocumentoUpdated(item);
            this.context.Documentos.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.Documentos.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "TipoArchivo,Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnDocumentoCreated(Models.Sgsst.Documento item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.Documento item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnDocumentoCreated(item);
            this.context.Documentos.Add(item);
            this.context.SaveChanges();

            var key = item.Id;

            var itemToReturn = this.context.Documentos.Where(i => i.Id == key);

            Request.QueryString = Request.QueryString.Add("$expand", "TipoArchivo,Empresa");

            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
