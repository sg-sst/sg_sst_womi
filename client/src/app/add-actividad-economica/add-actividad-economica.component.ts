import { Component, Injector } from '@angular/core';
import { AddActividadEconomicaGenerated } from './add-actividad-economica-generated.component';

@Component({
  selector: 'page-add-actividad-economica',
  templateUrl: './add-actividad-economica.component.html'
})
export class AddActividadEconomicaComponent extends AddActividadEconomicaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
