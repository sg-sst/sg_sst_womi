import { Component, Injector } from '@angular/core';
import { InfoDemograficasGenerated } from './info-demograficas-generated.component';

@Component({
  selector: 'page-info-demograficas',
  templateUrl: './info-demograficas.component.html'
})
export class InfoDemograficasComponent extends InfoDemograficasGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
