using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/IntensidadDeportes")]
  [Route("mvc/odata/sgsst/IntensidadDeportes")]
  public partial class IntensidadDeportesController : ODataController
  {
    private Data.SgsstContext context;

    public IntensidadDeportesController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/IntensidadDeportes
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.IntensidadDeporte> GetIntensidadDeportes()
    {
      var items = this.context.IntensidadDeportes.AsQueryable<Models.Sgsst.IntensidadDeporte>();
      this.OnIntensidadDeportesRead(ref items);

      return items;
    }

    partial void OnIntensidadDeportesRead(ref IQueryable<Models.Sgsst.IntensidadDeporte> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<IntensidadDeporte> GetIntensidadDeporte(Int64 key)
    {
        var items = this.context.IntensidadDeportes.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnIntensidadDeporteDeleted(Models.Sgsst.IntensidadDeporte item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteIntensidadDeporte(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.IntensidadDeportes
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.IntensidadDeporte>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnIntensidadDeporteDeleted(item);
            this.context.IntensidadDeportes.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnIntensidadDeporteUpdated(Models.Sgsst.IntensidadDeporte item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutIntensidadDeporte(Int64 key, [FromBody]Models.Sgsst.IntensidadDeporte newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.IntensidadDeportes
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.IntensidadDeporte>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnIntensidadDeporteUpdated(newItem);
            this.context.IntensidadDeportes.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.IntensidadDeportes.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchIntensidadDeporte(Int64 key, [FromBody]Delta<Models.Sgsst.IntensidadDeporte> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.IntensidadDeportes.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.IntensidadDeporte>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnIntensidadDeporteUpdated(item);
            this.context.IntensidadDeportes.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.IntensidadDeportes.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnIntensidadDeporteCreated(Models.Sgsst.IntensidadDeporte item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.IntensidadDeporte item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnIntensidadDeporteCreated(item);
            this.context.IntensidadDeportes.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/IntensidadDeportes/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
