import { Component, Injector } from '@angular/core';
import { EnfermedadesInfoDemofraficasGenerated } from './enfermedades-info-demofraficas-generated.component';

@Component({
  selector: 'page-enfermedades-info-demofraficas',
  templateUrl: './enfermedades-info-demofraficas.component.html'
})
export class EnfermedadesInfoDemofraficasComponent extends EnfermedadesInfoDemofraficasGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
