import { Component, Injector } from '@angular/core';
import { EditTipoContratoGenerated } from './edit-tipo-contrato-generated.component';

@Component({
  selector: 'page-edit-tipo-contrato',
  templateUrl: './edit-tipo-contrato.component.html'
})
export class EditTipoContratoComponent extends EditTipoContratoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
