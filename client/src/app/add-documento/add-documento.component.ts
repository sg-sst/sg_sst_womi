import { Component, Injector } from '@angular/core';
import { AddDocumentoGenerated } from './add-documento-generated.component';

@Component({
  selector: 'page-add-documento',
  templateUrl: './add-documento.component.html'
})
export class AddDocumentoComponent extends AddDocumentoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
