import { Component, Injector } from '@angular/core';
import { AddTiempoLibreGenerated } from './add-tiempo-libre-generated.component';

@Component({
  selector: 'page-add-tiempo-libre',
  templateUrl: './add-tiempo-libre.component.html'
})
export class AddTiempoLibreComponent extends AddTiempoLibreGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
