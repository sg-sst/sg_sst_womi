using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Tipo_Contrato", Schema = "public")]
  public partial class TipoContrato
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("TipoContrato")]
    public ICollection<InfoDemografica> InfoDemograficas { get; set; }
  }
}
