import { Component, Injector } from '@angular/core';
import { ActividadesBienestarsGenerated } from './actividades-bienestars-generated.component';

@Component({
  selector: 'page-actividades-bienestars',
  templateUrl: './actividades-bienestars.component.html'
})
export class ActividadesBienestarsComponent extends ActividadesBienestarsGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
