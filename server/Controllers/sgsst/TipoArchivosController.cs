using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/TipoArchivos")]
  [Route("mvc/odata/sgsst/TipoArchivos")]
  public partial class TipoArchivosController : ODataController
  {
    private Data.SgsstContext context;

    public TipoArchivosController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/TipoArchivos
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.TipoArchivo> GetTipoArchivos()
    {
      var items = this.context.TipoArchivos.AsQueryable<Models.Sgsst.TipoArchivo>();
      this.OnTipoArchivosRead(ref items);

      return items;
    }

    partial void OnTipoArchivosRead(ref IQueryable<Models.Sgsst.TipoArchivo> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<TipoArchivo> GetTipoArchivo(Int64 key)
    {
        var items = this.context.TipoArchivos.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnTipoArchivoDeleted(Models.Sgsst.TipoArchivo item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteTipoArchivo(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.TipoArchivos
                .Where(i => i.Id == key)
                .Include(i => i.Documentos)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.TipoArchivo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnTipoArchivoDeleted(item);
            this.context.TipoArchivos.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnTipoArchivoUpdated(Models.Sgsst.TipoArchivo item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutTipoArchivo(Int64 key, [FromBody]Models.Sgsst.TipoArchivo newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TipoArchivos
                .Where(i => i.Id == key)
                .Include(i => i.Documentos)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.TipoArchivo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnTipoArchivoUpdated(newItem);
            this.context.TipoArchivos.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.TipoArchivos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchTipoArchivo(Int64 key, [FromBody]Delta<Models.Sgsst.TipoArchivo> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TipoArchivos.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.TipoArchivo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnTipoArchivoUpdated(item);
            this.context.TipoArchivos.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.TipoArchivos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnTipoArchivoCreated(Models.Sgsst.TipoArchivo item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.TipoArchivo item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnTipoArchivoCreated(item);
            this.context.TipoArchivos.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/TipoArchivos/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
