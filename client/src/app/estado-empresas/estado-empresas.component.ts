import { Component, Injector } from '@angular/core';
import { EstadoEmpresasGenerated } from './estado-empresas-generated.component';

@Component({
  selector: 'page-estado-empresas',
  templateUrl: './estado-empresas.component.html'
})
export class EstadoEmpresasComponent extends EstadoEmpresasGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
