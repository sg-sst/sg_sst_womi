import { Component, Injector } from '@angular/core';
import { AddRangoIngresoGenerated } from './add-rango-ingreso-generated.component';

@Component({
  selector: 'page-add-rango-ingreso',
  templateUrl: './add-rango-ingreso.component.html'
})
export class AddRangoIngresoComponent extends AddRangoIngresoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
