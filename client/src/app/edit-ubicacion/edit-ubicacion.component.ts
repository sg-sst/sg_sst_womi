import { Component, Injector } from '@angular/core';
import { EditUbicacionGenerated } from './edit-ubicacion-generated.component';

@Component({
  selector: 'page-edit-ubicacion',
  templateUrl: './edit-ubicacion.component.html'
})
export class EditUbicacionComponent extends EditUbicacionGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
