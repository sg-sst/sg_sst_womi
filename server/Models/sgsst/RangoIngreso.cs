using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Rango_Ingresos", Schema = "public")]
  public partial class RangoIngreso
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("RangoIngreso")]
    public ICollection<InfoDemografica> InfoDemograficas { get; set; }
  }
}
