using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Estado_Civil", Schema = "public")]
  public partial class EstadoCivil
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("EstadoCivil")]
    public ICollection<InfoDemografica> InfoDemograficas { get; set; }
  }
}
