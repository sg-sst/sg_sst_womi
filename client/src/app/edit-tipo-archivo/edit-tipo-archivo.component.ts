import { Component, Injector } from '@angular/core';
import { EditTipoArchivoGenerated } from './edit-tipo-archivo-generated.component';

@Component({
  selector: 'page-edit-tipo-archivo',
  templateUrl: './edit-tipo-archivo.component.html'
})
export class EditTipoArchivoComponent extends EditTipoArchivoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
