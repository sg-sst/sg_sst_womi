using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Promedio_Ingresos", Schema = "public")]
  public partial class PromedioIngreso
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("PromedioIngreso")]
    public ICollection<InfoDemografica> InfoDemograficas { get; set; }
  }
}
