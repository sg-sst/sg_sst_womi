using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;

namespace Sgsst
{
  public class Program
  {
      public static void Main(string[] args)
      {
          var host = BuildWebHost(args);

          host.Run();
      }

      public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel(options => options.Listen(System.Net.IPAddress.Any, 5000, listenOptions =>
                    listenOptions.UseHttps("server.pfx", "radzen-https-password")))
              .UseUrls("https://*:5000")
              .UseStartup<Startup>()
              .Build();
  }
}
