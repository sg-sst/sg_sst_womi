using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/PromedioIngresos")]
  [Route("mvc/odata/sgsst/PromedioIngresos")]
  public partial class PromedioIngresosController : ODataController
  {
    private Data.SgsstContext context;

    public PromedioIngresosController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/PromedioIngresos
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.PromedioIngreso> GetPromedioIngresos()
    {
      var items = this.context.PromedioIngresos.AsQueryable<Models.Sgsst.PromedioIngreso>();
      this.OnPromedioIngresosRead(ref items);

      return items;
    }

    partial void OnPromedioIngresosRead(ref IQueryable<Models.Sgsst.PromedioIngreso> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<PromedioIngreso> GetPromedioIngreso(Int64 key)
    {
        var items = this.context.PromedioIngresos.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnPromedioIngresoDeleted(Models.Sgsst.PromedioIngreso item);

    [HttpDelete("{Id}")]
    public IActionResult DeletePromedioIngreso(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.PromedioIngresos
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.PromedioIngreso>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnPromedioIngresoDeleted(item);
            this.context.PromedioIngresos.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnPromedioIngresoUpdated(Models.Sgsst.PromedioIngreso item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutPromedioIngreso(Int64 key, [FromBody]Models.Sgsst.PromedioIngreso newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.PromedioIngresos
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.PromedioIngreso>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnPromedioIngresoUpdated(newItem);
            this.context.PromedioIngresos.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.PromedioIngresos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchPromedioIngreso(Int64 key, [FromBody]Delta<Models.Sgsst.PromedioIngreso> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.PromedioIngresos.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.PromedioIngreso>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnPromedioIngresoUpdated(item);
            this.context.PromedioIngresos.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.PromedioIngresos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnPromedioIngresoCreated(Models.Sgsst.PromedioIngreso item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.PromedioIngreso item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnPromedioIngresoCreated(item);
            this.context.PromedioIngresos.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/PromedioIngresos/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
