import { Component, Injector } from '@angular/core';
import { EditIntensidadDeporteGenerated } from './edit-intensidad-deporte-generated.component';

@Component({
  selector: 'page-edit-intensidad-deporte',
  templateUrl: './edit-intensidad-deporte.component.html'
})
export class EditIntensidadDeporteComponent extends EditIntensidadDeporteGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
