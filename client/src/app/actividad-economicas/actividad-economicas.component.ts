import { Component, Injector } from '@angular/core';
import { ActividadEconomicasGenerated } from './actividad-economicas-generated.component';

@Component({
  selector: 'page-actividad-economicas',
  templateUrl: './actividad-economicas.component.html'
})
export class ActividadEconomicasComponent extends ActividadEconomicasGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
