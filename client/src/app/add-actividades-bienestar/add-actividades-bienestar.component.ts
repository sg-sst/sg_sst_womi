import { Component, Injector } from '@angular/core';
import { AddActividadesBienestarGenerated } from './add-actividades-bienestar-generated.component';

@Component({
  selector: 'page-add-actividades-bienestar',
  templateUrl: './add-actividades-bienestar.component.html'
})
export class AddActividadesBienestarComponent extends AddActividadesBienestarGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
