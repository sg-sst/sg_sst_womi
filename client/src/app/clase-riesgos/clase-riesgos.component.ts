import { Component, Injector } from '@angular/core';
import { ClaseRiesgosGenerated } from './clase-riesgos-generated.component';

@Component({
  selector: 'page-clase-riesgos',
  templateUrl: './clase-riesgos.component.html'
})
export class ClaseRiesgosComponent extends ClaseRiesgosGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
