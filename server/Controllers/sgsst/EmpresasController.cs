using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/Empresas")]
  [Route("mvc/odata/sgsst/Empresas")]
  public partial class EmpresasController : ODataController
  {
    private Data.SgsstContext context;

    public EmpresasController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/Empresas
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.Empresa> GetEmpresas()
    {
      var items = this.context.Empresas.AsQueryable<Models.Sgsst.Empresa>();
      this.OnEmpresasRead(ref items);

      return items;
    }

    partial void OnEmpresasRead(ref IQueryable<Models.Sgsst.Empresa> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<Empresa> GetEmpresa(Int64 key)
    {
        var items = this.context.Empresas.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnEmpresaDeleted(Models.Sgsst.Empresa item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteEmpresa(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.Empresas
                .Where(i => i.Id == key)
                .Include(i => i.Documentos)
                .Include(i => i.Personas)
                .Include(i => i.EmpresaClaseRisgos)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Empresa>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEmpresaDeleted(item);
            this.context.Empresas.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEmpresaUpdated(Models.Sgsst.Empresa item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutEmpresa(Int64 key, [FromBody]Models.Sgsst.Empresa newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Empresas
                .Where(i => i.Id == key)
                .Include(i => i.Documentos)
                .Include(i => i.Personas)
                .Include(i => i.EmpresaClaseRisgos)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Empresa>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEmpresaUpdated(newItem);
            this.context.Empresas.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.Empresas.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "ActividadEconomica,Ubicacion,EstadoEmpresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchEmpresa(Int64 key, [FromBody]Delta<Models.Sgsst.Empresa> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Empresas.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.Empresa>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnEmpresaUpdated(item);
            this.context.Empresas.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.Empresas.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "ActividadEconomica,Ubicacion,EstadoEmpresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEmpresaCreated(Models.Sgsst.Empresa item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.Empresa item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnEmpresaCreated(item);
            this.context.Empresas.Add(item);
            this.context.SaveChanges();

            var key = item.Id;

            var itemToReturn = this.context.Empresas.Where(i => i.Id == key);

            Request.QueryString = Request.QueryString.Add("$expand", "ActividadEconomica,Ubicacion,EstadoEmpresa");

            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
