import { Component, Injector } from '@angular/core';
import { EditPersonaGenerated } from './edit-persona-generated.component';

@Component({
  selector: 'page-edit-persona',
  templateUrl: './edit-persona.component.html'
})
export class EditPersonaComponent extends EditPersonaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
