using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/ActividadEconomicas")]
  [Route("mvc/odata/sgsst/ActividadEconomicas")]
  public partial class ActividadEconomicasController : ODataController
  {
    private Data.SgsstContext context;

    public ActividadEconomicasController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/ActividadEconomicas
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.ActividadEconomica> GetActividadEconomicas()
    {
      var items = this.context.ActividadEconomicas.AsQueryable<Models.Sgsst.ActividadEconomica>();
      this.OnActividadEconomicasRead(ref items);

      return items;
    }

    partial void OnActividadEconomicasRead(ref IQueryable<Models.Sgsst.ActividadEconomica> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<ActividadEconomica> GetActividadEconomica(Int64 key)
    {
        var items = this.context.ActividadEconomicas.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnActividadEconomicaDeleted(Models.Sgsst.ActividadEconomica item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteActividadEconomica(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.ActividadEconomicas
                .Where(i => i.Id == key)
                .Include(i => i.Empresas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadEconomica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnActividadEconomicaDeleted(item);
            this.context.ActividadEconomicas.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnActividadEconomicaUpdated(Models.Sgsst.ActividadEconomica item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutActividadEconomica(Int64 key, [FromBody]Models.Sgsst.ActividadEconomica newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ActividadEconomicas
                .Where(i => i.Id == key)
                .Include(i => i.Empresas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadEconomica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnActividadEconomicaUpdated(newItem);
            this.context.ActividadEconomicas.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.ActividadEconomicas.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchActividadEconomica(Int64 key, [FromBody]Delta<Models.Sgsst.ActividadEconomica> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ActividadEconomicas.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadEconomica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnActividadEconomicaUpdated(item);
            this.context.ActividadEconomicas.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.ActividadEconomicas.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnActividadEconomicaCreated(Models.Sgsst.ActividadEconomica item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.ActividadEconomica item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnActividadEconomicaCreated(item);
            this.context.ActividadEconomicas.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/ActividadEconomicas/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
