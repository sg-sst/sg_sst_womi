using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/ActividadesPersonas")]
  [Route("mvc/odata/sgsst/ActividadesPersonas")]
  public partial class ActividadesPersonasController : ODataController
  {
    private Data.SgsstContext context;

    public ActividadesPersonasController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/ActividadesPersonas
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.ActividadesPersona> GetActividadesPersonas()
    {
      var items = this.context.ActividadesPersonas.AsQueryable<Models.Sgsst.ActividadesPersona>();
      this.OnActividadesPersonasRead(ref items);

      return items;
    }

    partial void OnActividadesPersonasRead(ref IQueryable<Models.Sgsst.ActividadesPersona> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Actividad_Bienestar_Id},{Info_Demografica_Id}")]
    public SingleResult<ActividadesPersona> GetActividadesPersona([FromODataUri] Int64 keyActividad_Bienestar_Id,[FromODataUri] Int64 keyInfo_Demografica_Id)
    {
        var items = this.context.ActividadesPersonas.Where(i=>i.Actividad_Bienestar_Id == keyActividad_Bienestar_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);
        return SingleResult.Create(items);
    }
    partial void OnActividadesPersonaDeleted(Models.Sgsst.ActividadesPersona item);

    [HttpDelete("{Actividad_Bienestar_Id},{Info_Demografica_Id}")]
    public IActionResult DeleteActividadesPersona([FromODataUri] Int64 keyActividad_Bienestar_Id,[FromODataUri] Int64 keyInfo_Demografica_Id)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.ActividadesPersonas
                .Where(i => i.Actividad_Bienestar_Id == keyActividad_Bienestar_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadesPersona>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnActividadesPersonaDeleted(item);
            this.context.ActividadesPersonas.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnActividadesPersonaUpdated(Models.Sgsst.ActividadesPersona item);

    [HttpPut("{Actividad_Bienestar_Id},{Info_Demografica_Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutActividadesPersona([FromODataUri] Int64 keyActividad_Bienestar_Id,[FromODataUri] Int64 keyInfo_Demografica_Id, [FromBody]Models.Sgsst.ActividadesPersona newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ActividadesPersonas
                .Where(i => i.Actividad_Bienestar_Id == keyActividad_Bienestar_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadesPersona>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnActividadesPersonaUpdated(newItem);
            this.context.ActividadesPersonas.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.ActividadesPersonas.Where(i => i.Actividad_Bienestar_Id == keyActividad_Bienestar_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);
            Request.QueryString = Request.QueryString.Add("$expand", "ActividadesBienestar,InfoDemografica");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Actividad_Bienestar_Id},{Info_Demografica_Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchActividadesPersona([FromODataUri] Int64 keyActividad_Bienestar_Id,[FromODataUri] Int64 keyInfo_Demografica_Id, [FromBody]Delta<Models.Sgsst.ActividadesPersona> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ActividadesPersonas.Where(i => i.Actividad_Bienestar_Id == keyActividad_Bienestar_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadesPersona>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnActividadesPersonaUpdated(item);
            this.context.ActividadesPersonas.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.ActividadesPersonas.Where(i => i.Actividad_Bienestar_Id == keyActividad_Bienestar_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);
            Request.QueryString = Request.QueryString.Add("$expand", "ActividadesBienestar,InfoDemografica");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnActividadesPersonaCreated(Models.Sgsst.ActividadesPersona item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.ActividadesPersona item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnActividadesPersonaCreated(item);
            this.context.ActividadesPersonas.Add(item);
            this.context.SaveChanges();

            var keyActividad_Bienestar_Id = item.Actividad_Bienestar_Id;
            var keyInfo_Demografica_Id = item.Info_Demografica_Id;

            var itemToReturn = this.context.ActividadesPersonas.Where(i => i.Actividad_Bienestar_Id == keyActividad_Bienestar_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);

            Request.QueryString = Request.QueryString.Add("$expand", "ActividadesBienestar,InfoDemografica");

            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
