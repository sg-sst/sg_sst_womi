import { Component, Injector } from '@angular/core';
import { EditTiempoLibreGenerated } from './edit-tiempo-libre-generated.component';

@Component({
  selector: 'page-edit-tiempo-libre',
  templateUrl: './edit-tiempo-libre.component.html'
})
export class EditTiempoLibreComponent extends EditTiempoLibreGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
