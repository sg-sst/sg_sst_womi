import { Component, Injector } from '@angular/core';
import { EditEmpresaGenerated } from './edit-empresa-generated.component';

@Component({
  selector: 'page-edit-empresa',
  templateUrl: './edit-empresa.component.html'
})
export class EditEmpresaComponent extends EditEmpresaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
