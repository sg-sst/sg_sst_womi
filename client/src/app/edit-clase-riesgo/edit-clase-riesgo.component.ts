import { Component, Injector } from '@angular/core';
import { EditClaseRiesgoGenerated } from './edit-clase-riesgo-generated.component';

@Component({
  selector: 'page-edit-clase-riesgo',
  templateUrl: './edit-clase-riesgo.component.html'
})
export class EditClaseRiesgoComponent extends EditClaseRiesgoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
