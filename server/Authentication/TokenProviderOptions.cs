﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace Sgsst.Authentication
{
    public class TokenProviderOptions
    {
        public static string Audience { get; } = "SgsstAudience";
        public static string Issuer { get; } = "Sgsst";
        public static SymmetricSecurityKey Key { get; } = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("SgsstSecretSecurityKeySgsst"));
        public static TimeSpan Expiration { get; } = TimeSpan.FromMinutes(5);
        public static SigningCredentials SigningCredentials { get; } = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256);
    }
}
