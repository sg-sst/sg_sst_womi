import { Component, Injector } from '@angular/core';
import { TiempoLibresGenerated } from './tiempo-libres-generated.component';

@Component({
  selector: 'page-tiempo-libres',
  templateUrl: './tiempo-libres.component.html'
})
export class TiempoLibresComponent extends TiempoLibresGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
