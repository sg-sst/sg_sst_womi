import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';

import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { ActividadEconomicasComponent } from './actividad-economicas/actividad-economicas.component';
import { AddActividadEconomicaComponent } from './add-actividad-economica/add-actividad-economica.component';
import { EditActividadEconomicaComponent } from './edit-actividad-economica/edit-actividad-economica.component';
import { ActividadesBienestarsComponent } from './actividades-bienestars/actividades-bienestars.component';
import { AddActividadesBienestarComponent } from './add-actividades-bienestar/add-actividades-bienestar.component';
import { EditActividadesBienestarComponent } from './edit-actividades-bienestar/edit-actividades-bienestar.component';
import { ActividadesPersonasComponent } from './actividades-personas/actividades-personas.component';
import { AddActividadesPersonaComponent } from './add-actividades-persona/add-actividades-persona.component';
import { EditActividadesPersonaComponent } from './edit-actividades-persona/edit-actividades-persona.component';
import { ClaseRiesgosComponent } from './clase-riesgos/clase-riesgos.component';
import { AddClaseRiesgoComponent } from './add-clase-riesgo/add-clase-riesgo.component';
import { EditClaseRiesgoComponent } from './edit-clase-riesgo/edit-clase-riesgo.component';
import { DocumentosComponent } from './documentos/documentos.component';
import { AddDocumentoComponent } from './add-documento/add-documento.component';
import { EditDocumentoComponent } from './edit-documento/edit-documento.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { AddEmpresaComponent } from './add-empresa/add-empresa.component';
import { EditEmpresaComponent } from './edit-empresa/edit-empresa.component';
import { EmpresaClaseRisgosComponent } from './empresa-clase-risgos/empresa-clase-risgos.component';
import { AddEmpresaClaseRisgoComponent } from './add-empresa-clase-risgo/add-empresa-clase-risgo.component';
import { EditEmpresaClaseRisgoComponent } from './edit-empresa-clase-risgo/edit-empresa-clase-risgo.component';
import { EnfermedadesComponent } from './enfermedades/enfermedades.component';
import { AddEnfermedadeComponent } from './add-enfermedade/add-enfermedade.component';
import { EditEnfermedadeComponent } from './edit-enfermedade/edit-enfermedade.component';
import { EnfermedadesInfoDemofraficasComponent } from './enfermedades-info-demofraficas/enfermedades-info-demofraficas.component';
import { AddEnfermedadesInfoDemofraficaComponent } from './add-enfermedades-info-demofrafica/add-enfermedades-info-demofrafica.component';
import { EditEnfermedadesInfoDemofraficaComponent } from './edit-enfermedades-info-demofrafica/edit-enfermedades-info-demofrafica.component';
import { EstadoCivilsComponent } from './estado-civils/estado-civils.component';
import { AddEstadoCivilComponent } from './add-estado-civil/add-estado-civil.component';
import { EditEstadoCivilComponent } from './edit-estado-civil/edit-estado-civil.component';
import { EstadoEmpresasComponent } from './estado-empresas/estado-empresas.component';
import { AddEstadoEmpresaComponent } from './add-estado-empresa/add-estado-empresa.component';
import { EditEstadoEmpresaComponent } from './edit-estado-empresa/edit-estado-empresa.component';
import { InfoDemograficasComponent } from './info-demograficas/info-demograficas.component';
import { AddInfoDemograficaComponent } from './add-info-demografica/add-info-demografica.component';
import { EditInfoDemograficaComponent } from './edit-info-demografica/edit-info-demografica.component';
import { IntensidadDeportesComponent } from './intensidad-deportes/intensidad-deportes.component';
import { AddIntensidadDeporteComponent } from './add-intensidad-deporte/add-intensidad-deporte.component';
import { EditIntensidadDeporteComponent } from './edit-intensidad-deporte/edit-intensidad-deporte.component';
import { PersonasComponent } from './personas/personas.component';
import { AddPersonaComponent } from './add-persona/add-persona.component';
import { EditPersonaComponent } from './edit-persona/edit-persona.component';
import { PromedioIngresosComponent } from './promedio-ingresos/promedio-ingresos.component';
import { AddPromedioIngresoComponent } from './add-promedio-ingreso/add-promedio-ingreso.component';
import { EditPromedioIngresoComponent } from './edit-promedio-ingreso/edit-promedio-ingreso.component';
import { RangoIngresosComponent } from './rango-ingresos/rango-ingresos.component';
import { AddRangoIngresoComponent } from './add-rango-ingreso/add-rango-ingreso.component';
import { EditRangoIngresoComponent } from './edit-rango-ingreso/edit-rango-ingreso.component';
import { TiempoLibresComponent } from './tiempo-libres/tiempo-libres.component';
import { AddTiempoLibreComponent } from './add-tiempo-libre/add-tiempo-libre.component';
import { EditTiempoLibreComponent } from './edit-tiempo-libre/edit-tiempo-libre.component';
import { TipoArchivosComponent } from './tipo-archivos/tipo-archivos.component';
import { AddTipoArchivoComponent } from './add-tipo-archivo/add-tipo-archivo.component';
import { EditTipoArchivoComponent } from './edit-tipo-archivo/edit-tipo-archivo.component';
import { TipoContratosComponent } from './tipo-contratos/tipo-contratos.component';
import { AddTipoContratoComponent } from './add-tipo-contrato/add-tipo-contrato.component';
import { EditTipoContratoComponent } from './edit-tipo-contrato/edit-tipo-contrato.component';
import { UbicacionsComponent } from './ubicacions/ubicacions.component';
import { AddUbicacionComponent } from './add-ubicacion/add-ubicacion.component';
import { EditUbicacionComponent } from './edit-ubicacion/edit-ubicacion.component';
import { LoginComponent } from './login/login.component';
import { ApplicationUsersComponent } from './application-users/application-users.component';
import { ApplicationRolesComponent } from './application-roles/application-roles.component';
import { RegisterApplicationUserComponent } from './register-application-user/register-application-user.component';
import { AddApplicationRoleComponent } from './add-application-role/add-application-role.component';
import { AddApplicationUserComponent } from './add-application-user/add-application-user.component';
import { EditApplicationUserComponent } from './edit-application-user/edit-application-user.component';
import { ProfileComponent } from './profile/profile.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';

import { SecurityService } from './security.service';
import { AuthGuard } from './auth.guard';
export const routes: Routes = [
  { path: '', redirectTo: '/actividad-economicas', pathMatch: 'full' },
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'actividad-economicas',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: ActividadEconomicasComponent
      },
      {
        path: 'add-actividad-economica',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddActividadEconomicaComponent
      },
      {
        path: 'edit-actividad-economica/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditActividadEconomicaComponent
      },
      {
        path: 'actividades-bienestars',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: ActividadesBienestarsComponent
      },
      {
        path: 'add-actividades-bienestar',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddActividadesBienestarComponent
      },
      {
        path: 'edit-actividades-bienestar/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditActividadesBienestarComponent
      },
      {
        path: 'actividades-personas',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: ActividadesPersonasComponent
      },
      {
        path: 'add-actividades-persona',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddActividadesPersonaComponent
      },
      {
        path: 'edit-actividades-persona/:Actividad_Bienestar_Id/:Info_Demografica_Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditActividadesPersonaComponent
      },
      {
        path: 'clase-riesgos',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: ClaseRiesgosComponent
      },
      {
        path: 'add-clase-riesgo',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddClaseRiesgoComponent
      },
      {
        path: 'edit-clase-riesgo/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditClaseRiesgoComponent
      },
      {
        path: 'documentos',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: DocumentosComponent
      },
      {
        path: 'add-documento',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddDocumentoComponent
      },
      {
        path: 'edit-documento/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditDocumentoComponent
      },
      {
        path: 'empresas',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EmpresasComponent
      },
      {
        path: 'add-empresa',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddEmpresaComponent
      },
      {
        path: 'edit-empresa/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditEmpresaComponent
      },
      {
        path: 'empresa-clase-risgos',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EmpresaClaseRisgosComponent
      },
      {
        path: 'add-empresa-clase-risgo',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddEmpresaClaseRisgoComponent
      },
      {
        path: 'edit-empresa-clase-risgo/:Empresa_Id/:Clase_Riesgo_Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditEmpresaClaseRisgoComponent
      },
      {
        path: 'enfermedades',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EnfermedadesComponent
      },
      {
        path: 'add-enfermedade',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddEnfermedadeComponent
      },
      {
        path: 'edit-enfermedade/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditEnfermedadeComponent
      },
      {
        path: 'enfermedades-info-demofraficas',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EnfermedadesInfoDemofraficasComponent
      },
      {
        path: 'add-enfermedades-info-demofrafica',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddEnfermedadesInfoDemofraficaComponent
      },
      {
        path: 'edit-enfermedades-info-demofrafica/:Enfermedad_Id/:Info_Demografica_Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditEnfermedadesInfoDemofraficaComponent
      },
      {
        path: 'estado-civils',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EstadoCivilsComponent
      },
      {
        path: 'add-estado-civil',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddEstadoCivilComponent
      },
      {
        path: 'edit-estado-civil/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditEstadoCivilComponent
      },
      {
        path: 'estado-empresas',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EstadoEmpresasComponent
      },
      {
        path: 'add-estado-empresa',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddEstadoEmpresaComponent
      },
      {
        path: 'edit-estado-empresa/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditEstadoEmpresaComponent
      },
      {
        path: 'info-demograficas',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: InfoDemograficasComponent
      },
      {
        path: 'add-info-demografica',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddInfoDemograficaComponent
      },
      {
        path: 'edit-info-demografica/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditInfoDemograficaComponent
      },
      {
        path: 'intensidad-deportes',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: IntensidadDeportesComponent
      },
      {
        path: 'add-intensidad-deporte',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddIntensidadDeporteComponent
      },
      {
        path: 'edit-intensidad-deporte/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditIntensidadDeporteComponent
      },
      {
        path: 'personas',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: PersonasComponent
      },
      {
        path: 'add-persona',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddPersonaComponent
      },
      {
        path: 'edit-persona/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditPersonaComponent
      },
      {
        path: 'promedio-ingresos',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: PromedioIngresosComponent
      },
      {
        path: 'add-promedio-ingreso',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddPromedioIngresoComponent
      },
      {
        path: 'edit-promedio-ingreso/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditPromedioIngresoComponent
      },
      {
        path: 'rango-ingresos',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: RangoIngresosComponent
      },
      {
        path: 'add-rango-ingreso',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddRangoIngresoComponent
      },
      {
        path: 'edit-rango-ingreso/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditRangoIngresoComponent
      },
      {
        path: 'tiempo-libres',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: TiempoLibresComponent
      },
      {
        path: 'add-tiempo-libre',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddTiempoLibreComponent
      },
      {
        path: 'edit-tiempo-libre/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditTiempoLibreComponent
      },
      {
        path: 'tipo-archivos',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: TipoArchivosComponent
      },
      {
        path: 'add-tipo-archivo',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddTipoArchivoComponent
      },
      {
        path: 'edit-tipo-archivo/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditTipoArchivoComponent
      },
      {
        path: 'tipo-contratos',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: TipoContratosComponent
      },
      {
        path: 'add-tipo-contrato',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddTipoContratoComponent
      },
      {
        path: 'edit-tipo-contrato/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditTipoContratoComponent
      },
      {
        path: 'ubicacions',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: UbicacionsComponent
      },
      {
        path: 'add-ubicacion',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddUbicacionComponent
      },
      {
        path: 'edit-ubicacion/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditUbicacionComponent
      },
      {
        path: 'application-users',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: ApplicationUsersComponent
      },
      {
        path: 'application-roles',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: ApplicationRolesComponent
      },
      {
        path: 'register-application-user',
        data: {
          roles: ['Everybody'],
        },
        component: RegisterApplicationUserComponent
      },
      {
        path: 'add-application-role',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddApplicationRoleComponent
      },
      {
        path: 'add-application-user',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: AddApplicationUserComponent
      },
      {
        path: 'edit-application-user/:Id',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: EditApplicationUserComponent
      },
      {
        path: 'profile',
        canActivate: [AuthGuard],
        data: {
          roles: ['Authenticated'],
        },
        component: ProfileComponent
      },
      {
        path: 'unauthorized',
        data: {
          roles: ['Everybody'],
        },
        component: UnauthorizedComponent
      },
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        data: {
          roles: ['Everybody'],
        },
        component: LoginComponent
      },
    ]
  },
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
