import { Component, Injector } from '@angular/core';
import { EditActividadesPersonaGenerated } from './edit-actividades-persona-generated.component';

@Component({
  selector: 'page-edit-actividades-persona',
  templateUrl: './edit-actividades-persona.component.html'
})
export class EditActividadesPersonaComponent extends EditActividadesPersonaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
