import { Component, Injector } from '@angular/core';
import { AddPersonaGenerated } from './add-persona-generated.component';

@Component({
  selector: 'page-add-persona',
  templateUrl: './add-persona.component.html'
})
export class AddPersonaComponent extends AddPersonaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
