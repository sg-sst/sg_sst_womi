using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Clase_Riesgo", Schema = "public")]
  public partial class ClaseRiesgo
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("ClaseRiesgo")]
    public ICollection<InfoDemografica> InfoDemograficas { get; set; }

    [InverseProperty("ClaseRiesgo")]
    public ICollection<EmpresaClaseRisgo> EmpresaClaseRisgos { get; set; }
  }
}
