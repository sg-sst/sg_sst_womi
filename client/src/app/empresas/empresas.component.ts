import { Component, Injector } from '@angular/core';
import { EmpresasGenerated } from './empresas-generated.component';

@Component({
  selector: 'page-empresas',
  templateUrl: './empresas.component.html'
})
export class EmpresasComponent extends EmpresasGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
