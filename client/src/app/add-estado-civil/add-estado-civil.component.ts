import { Component, Injector } from '@angular/core';
import { AddEstadoCivilGenerated } from './add-estado-civil-generated.component';

@Component({
  selector: 'page-add-estado-civil',
  templateUrl: './add-estado-civil.component.html'
})
export class AddEstadoCivilComponent extends AddEstadoCivilGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
