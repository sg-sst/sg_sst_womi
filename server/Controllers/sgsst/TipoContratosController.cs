using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/TipoContratos")]
  [Route("mvc/odata/sgsst/TipoContratos")]
  public partial class TipoContratosController : ODataController
  {
    private Data.SgsstContext context;

    public TipoContratosController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/TipoContratos
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.TipoContrato> GetTipoContratos()
    {
      var items = this.context.TipoContratos.AsQueryable<Models.Sgsst.TipoContrato>();
      this.OnTipoContratosRead(ref items);

      return items;
    }

    partial void OnTipoContratosRead(ref IQueryable<Models.Sgsst.TipoContrato> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<TipoContrato> GetTipoContrato(Int64 key)
    {
        var items = this.context.TipoContratos.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnTipoContratoDeleted(Models.Sgsst.TipoContrato item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteTipoContrato(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.TipoContratos
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.TipoContrato>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnTipoContratoDeleted(item);
            this.context.TipoContratos.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnTipoContratoUpdated(Models.Sgsst.TipoContrato item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutTipoContrato(Int64 key, [FromBody]Models.Sgsst.TipoContrato newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TipoContratos
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.TipoContrato>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnTipoContratoUpdated(newItem);
            this.context.TipoContratos.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.TipoContratos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchTipoContrato(Int64 key, [FromBody]Delta<Models.Sgsst.TipoContrato> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TipoContratos.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.TipoContrato>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnTipoContratoUpdated(item);
            this.context.TipoContratos.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.TipoContratos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnTipoContratoCreated(Models.Sgsst.TipoContrato item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.TipoContrato item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnTipoContratoCreated(item);
            this.context.TipoContratos.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/TipoContratos/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
