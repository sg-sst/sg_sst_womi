import { Component, Injector } from '@angular/core';
import { AddEmpresaGenerated } from './add-empresa-generated.component';

@Component({
  selector: 'page-add-empresa',
  templateUrl: './add-empresa.component.html'
})
export class AddEmpresaComponent extends AddEmpresaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
