using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/EstadoEmpresas")]
  [Route("mvc/odata/sgsst/EstadoEmpresas")]
  public partial class EstadoEmpresasController : ODataController
  {
    private Data.SgsstContext context;

    public EstadoEmpresasController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/EstadoEmpresas
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.EstadoEmpresa> GetEstadoEmpresas()
    {
      var items = this.context.EstadoEmpresas.AsQueryable<Models.Sgsst.EstadoEmpresa>();
      this.OnEstadoEmpresasRead(ref items);

      return items;
    }

    partial void OnEstadoEmpresasRead(ref IQueryable<Models.Sgsst.EstadoEmpresa> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<EstadoEmpresa> GetEstadoEmpresa(Int64 key)
    {
        var items = this.context.EstadoEmpresas.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnEstadoEmpresaDeleted(Models.Sgsst.EstadoEmpresa item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteEstadoEmpresa(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.EstadoEmpresas
                .Where(i => i.Id == key)
                .Include(i => i.Empresas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.EstadoEmpresa>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEstadoEmpresaDeleted(item);
            this.context.EstadoEmpresas.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEstadoEmpresaUpdated(Models.Sgsst.EstadoEmpresa item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutEstadoEmpresa(Int64 key, [FromBody]Models.Sgsst.EstadoEmpresa newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.EstadoEmpresas
                .Where(i => i.Id == key)
                .Include(i => i.Empresas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.EstadoEmpresa>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEstadoEmpresaUpdated(newItem);
            this.context.EstadoEmpresas.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.EstadoEmpresas.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchEstadoEmpresa(Int64 key, [FromBody]Delta<Models.Sgsst.EstadoEmpresa> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.EstadoEmpresas.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.EstadoEmpresa>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnEstadoEmpresaUpdated(item);
            this.context.EstadoEmpresas.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.EstadoEmpresas.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEstadoEmpresaCreated(Models.Sgsst.EstadoEmpresa item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.EstadoEmpresa item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnEstadoEmpresaCreated(item);
            this.context.EstadoEmpresas.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/EstadoEmpresas/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
