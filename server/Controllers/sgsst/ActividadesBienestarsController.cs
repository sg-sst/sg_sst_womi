using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/ActividadesBienestars")]
  [Route("mvc/odata/sgsst/ActividadesBienestars")]
  public partial class ActividadesBienestarsController : ODataController
  {
    private Data.SgsstContext context;

    public ActividadesBienestarsController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/ActividadesBienestars
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.ActividadesBienestar> GetActividadesBienestars()
    {
      var items = this.context.ActividadesBienestars.AsQueryable<Models.Sgsst.ActividadesBienestar>();
      this.OnActividadesBienestarsRead(ref items);

      return items;
    }

    partial void OnActividadesBienestarsRead(ref IQueryable<Models.Sgsst.ActividadesBienestar> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<ActividadesBienestar> GetActividadesBienestar(Int64 key)
    {
        var items = this.context.ActividadesBienestars.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnActividadesBienestarDeleted(Models.Sgsst.ActividadesBienestar item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteActividadesBienestar(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.ActividadesBienestars
                .Where(i => i.Id == key)
                .Include(i => i.ActividadesPersonas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadesBienestar>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnActividadesBienestarDeleted(item);
            this.context.ActividadesBienestars.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnActividadesBienestarUpdated(Models.Sgsst.ActividadesBienestar item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutActividadesBienestar(Int64 key, [FromBody]Models.Sgsst.ActividadesBienestar newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ActividadesBienestars
                .Where(i => i.Id == key)
                .Include(i => i.ActividadesPersonas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadesBienestar>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnActividadesBienestarUpdated(newItem);
            this.context.ActividadesBienestars.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.ActividadesBienestars.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchActividadesBienestar(Int64 key, [FromBody]Delta<Models.Sgsst.ActividadesBienestar> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ActividadesBienestars.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.ActividadesBienestar>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnActividadesBienestarUpdated(item);
            this.context.ActividadesBienestars.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.ActividadesBienestars.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnActividadesBienestarCreated(Models.Sgsst.ActividadesBienestar item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.ActividadesBienestar item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnActividadesBienestarCreated(item);
            this.context.ActividadesBienestars.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/ActividadesBienestars/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
