import { Component, Injector } from '@angular/core';
import { PromedioIngresosGenerated } from './promedio-ingresos-generated.component';

@Component({
  selector: 'page-promedio-ingresos',
  templateUrl: './promedio-ingresos.component.html'
})
export class PromedioIngresosComponent extends PromedioIngresosGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
