import { Component, Injector } from '@angular/core';
import { AddEmpresaClaseRisgoGenerated } from './add-empresa-clase-risgo-generated.component';

@Component({
  selector: 'page-add-empresa-clase-risgo',
  templateUrl: './add-empresa-clase-risgo.component.html'
})
export class AddEmpresaClaseRisgoComponent extends AddEmpresaClaseRisgoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
