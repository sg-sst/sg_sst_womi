using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Estado_Empresas", Schema = "public")]
  public partial class EstadoEmpresa
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("EstadoEmpresa")]
    public ICollection<Empresa> Empresas { get; set; }
  }
}
