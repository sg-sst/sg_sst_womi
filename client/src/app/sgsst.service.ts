import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';

import { ConfigService } from './config.service';
import { ODataClient } from './odata-client';
import * as models from './sgsst.model';

@Injectable()
export class SgsstService {
  odata: ODataClient;
  basePath: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.basePath = config.get('sgsst');
    this.odata = new ODataClient(this.http, this.basePath, { legacy: false, withCredentials: true });
  }

  getActividadEconomicas(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/ActividadEconomicas`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createActividadEconomica(expand: string | null, actividadEconomica: models.ActividadEconomica | null) : Observable<any> {
    return this.odata.post(`/ActividadEconomicas`, actividadEconomica, { expand }, []);
  }

  deleteActividadEconomica(id: number | null) : Observable<any> {
    return this.odata.delete(`/ActividadEconomicas(${id})`, item => !(item.Id == id));
  }

  getActividadEconomicaById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/ActividadEconomicas(${id})`, { expand });
  }

  updateActividadEconomica(expand: string | null, id: number | null, actividadEconomica: models.ActividadEconomica | null) : Observable<any> {
    return this.odata.patch(`/ActividadEconomicas(${id})`, actividadEconomica, item => item.Id == id, { expand }, []);
  }

  getActividadesBienestars(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/ActividadesBienestars`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createActividadesBienestar(expand: string | null, actividadesBienestar: models.ActividadesBienestar | null) : Observable<any> {
    return this.odata.post(`/ActividadesBienestars`, actividadesBienestar, { expand }, []);
  }

  deleteActividadesBienestar(id: number | null) : Observable<any> {
    return this.odata.delete(`/ActividadesBienestars(${id})`, item => !(item.Id == id));
  }

  getActividadesBienestarById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/ActividadesBienestars(${id})`, { expand });
  }

  updateActividadesBienestar(expand: string | null, id: number | null, actividadesBienestar: models.ActividadesBienestar | null) : Observable<any> {
    return this.odata.patch(`/ActividadesBienestars(${id})`, actividadesBienestar, item => item.Id == id, { expand }, []);
  }

  getActividadesPersonas(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/ActividadesPersonas`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createActividadesPersona(expand: string | null, actividadesPersona: models.ActividadesPersona | null) : Observable<any> {
    return this.odata.post(`/ActividadesPersonas`, actividadesPersona, { expand }, ['ActividadesBienestar', 'InfoDemografica']);
  }

  getClaseRiesgos(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/ClaseRiesgos`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createClaseRiesgo(expand: string | null, claseRiesgo: models.ClaseRiesgo | null) : Observable<any> {
    return this.odata.post(`/ClaseRiesgos`, claseRiesgo, { expand }, []);
  }

  deleteClaseRiesgo(id: number | null) : Observable<any> {
    return this.odata.delete(`/ClaseRiesgos(${id})`, item => !(item.Id == id));
  }

  getClaseRiesgoById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/ClaseRiesgos(${id})`, { expand });
  }

  updateClaseRiesgo(expand: string | null, id: number | null, claseRiesgo: models.ClaseRiesgo | null) : Observable<any> {
    return this.odata.patch(`/ClaseRiesgos(${id})`, claseRiesgo, item => item.Id == id, { expand }, []);
  }

  getDocumentos(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/Documentos`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createDocumento(expand: string | null, documento: models.Documento | null) : Observable<any> {
    return this.odata.post(`/Documentos`, documento, { expand }, ['TipoArchivo', 'Empresa']);
  }

  deleteDocumento(id: number | null) : Observable<any> {
    return this.odata.delete(`/Documentos(${id})`, item => !(item.Id == id));
  }

  getDocumentoById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/Documentos(${id})`, { expand });
  }

  updateDocumento(expand: string | null, id: number | null, documento: models.Documento | null) : Observable<any> {
    return this.odata.patch(`/Documentos(${id})`, documento, item => item.Id == id, { expand }, ['TipoArchivo','Empresa']);
  }

  getEmpresas(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/Empresas`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createEmpresa(expand: string | null, empresa: models.Empresa | null) : Observable<any> {
    return this.odata.post(`/Empresas`, empresa, { expand }, ['ActividadEconomica', 'Ubicacion', 'EstadoEmpresa']);
  }

  deleteEmpresa(id: number | null) : Observable<any> {
    return this.odata.delete(`/Empresas(${id})`, item => !(item.Id == id));
  }

  getEmpresaById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/Empresas(${id})`, { expand });
  }

  updateEmpresa(expand: string | null, id: number | null, empresa: models.Empresa | null) : Observable<any> {
    return this.odata.patch(`/Empresas(${id})`, empresa, item => item.Id == id, { expand }, ['ActividadEconomica','Ubicacion','EstadoEmpresa']);
  }

  getEmpresaClaseRisgos(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/EmpresaClaseRisgos`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createEmpresaClaseRisgo(expand: string | null, empresaClaseRisgo: models.EmpresaClaseRisgo | null) : Observable<any> {
    return this.odata.post(`/EmpresaClaseRisgos`, empresaClaseRisgo, { expand }, ['Empresa', 'ClaseRiesgo']);
  }

  getEnfermedades(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/Enfermedades`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createEnfermedade(expand: string | null, enfermedade: models.Enfermedade | null) : Observable<any> {
    return this.odata.post(`/Enfermedades`, enfermedade, { expand }, []);
  }

  deleteEnfermedade(id: number | null) : Observable<any> {
    return this.odata.delete(`/Enfermedades(${id})`, item => !(item.Id == id));
  }

  getEnfermedadeById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/Enfermedades(${id})`, { expand });
  }

  updateEnfermedade(expand: string | null, id: number | null, enfermedade: models.Enfermedade | null) : Observable<any> {
    return this.odata.patch(`/Enfermedades(${id})`, enfermedade, item => item.Id == id, { expand }, []);
  }

  getEnfermedadesInfoDemofraficas(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/EnfermedadesInfoDemofraficas`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createEnfermedadesInfoDemofrafica(expand: string | null, enfermedadesInfoDemofrafica: models.EnfermedadesInfoDemofrafica | null) : Observable<any> {
    return this.odata.post(`/EnfermedadesInfoDemofraficas`, enfermedadesInfoDemofrafica, { expand }, ['Enfermedade', 'InfoDemografica']);
  }

  getEstadoCivils(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/EstadoCivils`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createEstadoCivil(expand: string | null, estadoCivil: models.EstadoCivil | null) : Observable<any> {
    return this.odata.post(`/EstadoCivils`, estadoCivil, { expand }, []);
  }

  deleteEstadoCivil(id: number | null) : Observable<any> {
    return this.odata.delete(`/EstadoCivils(${id})`, item => !(item.Id == id));
  }

  getEstadoCivilById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/EstadoCivils(${id})`, { expand });
  }

  updateEstadoCivil(expand: string | null, id: number | null, estadoCivil: models.EstadoCivil | null) : Observable<any> {
    return this.odata.patch(`/EstadoCivils(${id})`, estadoCivil, item => item.Id == id, { expand }, []);
  }

  getEstadoEmpresas(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/EstadoEmpresas`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createEstadoEmpresa(expand: string | null, estadoEmpresa: models.EstadoEmpresa | null) : Observable<any> {
    return this.odata.post(`/EstadoEmpresas`, estadoEmpresa, { expand }, []);
  }

  deleteEstadoEmpresa(id: number | null) : Observable<any> {
    return this.odata.delete(`/EstadoEmpresas(${id})`, item => !(item.Id == id));
  }

  getEstadoEmpresaById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/EstadoEmpresas(${id})`, { expand });
  }

  updateEstadoEmpresa(expand: string | null, id: number | null, estadoEmpresa: models.EstadoEmpresa | null) : Observable<any> {
    return this.odata.patch(`/EstadoEmpresas(${id})`, estadoEmpresa, item => item.Id == id, { expand }, []);
  }

  getInfoDemograficas(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/InfoDemograficas`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createInfoDemografica(expand: string | null, infoDemografica: models.InfoDemografica | null) : Observable<any> {
    return this.odata.post(`/InfoDemograficas`, infoDemografica, { expand }, ['Persona', 'ClaseRiesgo', 'EstadoCivil', 'TiempoLibre', 'PromedioIngreso', 'RangoIngreso', 'TipoContrato', 'IntensidadDeporte']);
  }

  deleteInfoDemografica(id: number | null) : Observable<any> {
    return this.odata.delete(`/InfoDemograficas(${id})`, item => !(item.Id == id));
  }

  getInfoDemograficaById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/InfoDemograficas(${id})`, { expand });
  }

  updateInfoDemografica(expand: string | null, id: number | null, infoDemografica: models.InfoDemografica | null) : Observable<any> {
    return this.odata.patch(`/InfoDemograficas(${id})`, infoDemografica, item => item.Id == id, { expand }, ['Persona','ClaseRiesgo','EstadoCivil','TiempoLibre','PromedioIngreso','RangoIngreso','TipoContrato','IntensidadDeporte']);
  }

  getIntensidadDeportes(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/IntensidadDeportes`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createIntensidadDeporte(expand: string | null, intensidadDeporte: models.IntensidadDeporte | null) : Observable<any> {
    return this.odata.post(`/IntensidadDeportes`, intensidadDeporte, { expand }, []);
  }

  deleteIntensidadDeporte(id: number | null) : Observable<any> {
    return this.odata.delete(`/IntensidadDeportes(${id})`, item => !(item.Id == id));
  }

  getIntensidadDeporteById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/IntensidadDeportes(${id})`, { expand });
  }

  updateIntensidadDeporte(expand: string | null, id: number | null, intensidadDeporte: models.IntensidadDeporte | null) : Observable<any> {
    return this.odata.patch(`/IntensidadDeportes(${id})`, intensidadDeporte, item => item.Id == id, { expand }, []);
  }

  getPersonas(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/Personas`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createPersona(expand: string | null, persona: models.Persona | null) : Observable<any> {
    return this.odata.post(`/Personas`, persona, { expand }, ['Empresa']);
  }

  deletePersona(id: number | null) : Observable<any> {
    return this.odata.delete(`/Personas(${id})`, item => !(item.Id == id));
  }

  getPersonaById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/Personas(${id})`, { expand });
  }

  updatePersona(expand: string | null, id: number | null, persona: models.Persona | null) : Observable<any> {
    return this.odata.patch(`/Personas(${id})`, persona, item => item.Id == id, { expand }, ['Empresa']);
  }

  getPromedioIngresos(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/PromedioIngresos`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createPromedioIngreso(expand: string | null, promedioIngreso: models.PromedioIngreso | null) : Observable<any> {
    return this.odata.post(`/PromedioIngresos`, promedioIngreso, { expand }, []);
  }

  deletePromedioIngreso(id: number | null) : Observable<any> {
    return this.odata.delete(`/PromedioIngresos(${id})`, item => !(item.Id == id));
  }

  getPromedioIngresoById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/PromedioIngresos(${id})`, { expand });
  }

  updatePromedioIngreso(expand: string | null, id: number | null, promedioIngreso: models.PromedioIngreso | null) : Observable<any> {
    return this.odata.patch(`/PromedioIngresos(${id})`, promedioIngreso, item => item.Id == id, { expand }, []);
  }

  getRangoIngresos(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/RangoIngresos`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createRangoIngreso(expand: string | null, rangoIngreso: models.RangoIngreso | null) : Observable<any> {
    return this.odata.post(`/RangoIngresos`, rangoIngreso, { expand }, []);
  }

  deleteRangoIngreso(id: number | null) : Observable<any> {
    return this.odata.delete(`/RangoIngresos(${id})`, item => !(item.Id == id));
  }

  getRangoIngresoById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/RangoIngresos(${id})`, { expand });
  }

  updateRangoIngreso(expand: string | null, id: number | null, rangoIngreso: models.RangoIngreso | null) : Observable<any> {
    return this.odata.patch(`/RangoIngresos(${id})`, rangoIngreso, item => item.Id == id, { expand }, []);
  }

  getTiempoLibres(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/TiempoLibres`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createTiempoLibre(expand: string | null, tiempoLibre: models.TiempoLibre | null) : Observable<any> {
    return this.odata.post(`/TiempoLibres`, tiempoLibre, { expand }, []);
  }

  deleteTiempoLibre(id: number | null) : Observable<any> {
    return this.odata.delete(`/TiempoLibres(${id})`, item => !(item.Id == id));
  }

  getTiempoLibreById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/TiempoLibres(${id})`, { expand });
  }

  updateTiempoLibre(expand: string | null, id: number | null, tiempoLibre: models.TiempoLibre | null) : Observable<any> {
    return this.odata.patch(`/TiempoLibres(${id})`, tiempoLibre, item => item.Id == id, { expand }, []);
  }

  getTipoArchivos(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/TipoArchivos`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createTipoArchivo(expand: string | null, tipoArchivo: models.TipoArchivo | null) : Observable<any> {
    return this.odata.post(`/TipoArchivos`, tipoArchivo, { expand }, []);
  }

  deleteTipoArchivo(id: number | null) : Observable<any> {
    return this.odata.delete(`/TipoArchivos(${id})`, item => !(item.Id == id));
  }

  getTipoArchivoById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/TipoArchivos(${id})`, { expand });
  }

  updateTipoArchivo(expand: string | null, id: number | null, tipoArchivo: models.TipoArchivo | null) : Observable<any> {
    return this.odata.patch(`/TipoArchivos(${id})`, tipoArchivo, item => item.Id == id, { expand }, []);
  }

  getTipoContratos(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/TipoContratos`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createTipoContrato(expand: string | null, tipoContrato: models.TipoContrato | null) : Observable<any> {
    return this.odata.post(`/TipoContratos`, tipoContrato, { expand }, []);
  }

  deleteTipoContrato(id: number | null) : Observable<any> {
    return this.odata.delete(`/TipoContratos(${id})`, item => !(item.Id == id));
  }

  getTipoContratoById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/TipoContratos(${id})`, { expand });
  }

  updateTipoContrato(expand: string | null, id: number | null, tipoContrato: models.TipoContrato | null) : Observable<any> {
    return this.odata.patch(`/TipoContratos(${id})`, tipoContrato, item => item.Id == id, { expand }, []);
  }

  getUbicacions(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/Ubicacions`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createUbicacion(expand: string | null, ubicacion: models.Ubicacion | null) : Observable<any> {
    return this.odata.post(`/Ubicacions`, ubicacion, { expand }, ['Ubicacion1']);
  }

  deleteUbicacion(id: number | null) : Observable<any> {
    return this.odata.delete(`/Ubicacions(${id})`, item => !(item.Id == id));
  }

  getUbicacionById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/Ubicacions(${id})`, { expand });
  }

  updateUbicacion(expand: string | null, id: number | null, ubicacion: models.Ubicacion | null) : Observable<any> {
    return this.odata.patch(`/Ubicacions(${id})`, ubicacion, item => item.Id == id, { expand }, ['Ubicacion1']);
  }

  deleteActividadesPersona(actividadBienestarId: number | null, infoDemograficaId: number | null) : Observable<any> {
    return this.odata.delete(`/ActividadesPersonas(Actividad_Bienestar_Id=${actividadBienestarId},Info_Demografica_Id=${infoDemograficaId})`, item => !(item.Actividad_Bienestar_Id == actividadBienestarId && item.Info_Demografica_Id == infoDemograficaId));
  }

  getActividadesPersonaByActividadBienestarIdAndInfoDemograficaId(actividadBienestarId: number | null, infoDemograficaId: number | null, expand: string | null) : Observable<any> {
    return this.odata.getById(`/ActividadesPersonas(Actividad_Bienestar_Id=${actividadBienestarId},Info_Demografica_Id=${infoDemograficaId})`, { expand });
  }

  updateActividadesPersona(actividadBienestarId: number | null, infoDemograficaId: number | null, expand: string | null, actividadesPersona: models.ActividadesPersona | null) : Observable<any> {
    return this.odata.patch(`/ActividadesPersonas(Actividad_Bienestar_Id=${actividadBienestarId},Info_Demografica_Id=${infoDemograficaId})`, actividadesPersona, item => item.Actividad_Bienestar_Id == actividadBienestarId && item.Info_Demografica_Id == infoDemograficaId, { expand }, ['ActividadesBienestar','InfoDemografica']);
  }

  deleteEmpresaClaseRisgo(empresaId: number | null, claseRiesgoId: number | null) : Observable<any> {
    return this.odata.delete(`/EmpresaClaseRisgos(Empresa_Id=${empresaId},Clase_Riesgo_Id=${claseRiesgoId})`, item => !(item.Empresa_Id == empresaId && item.Clase_Riesgo_Id == claseRiesgoId));
  }

  getEmpresaClaseRisgoByEmpresaIdAndClaseRiesgoId(empresaId: number | null, claseRiesgoId: number | null, expand: string | null) : Observable<any> {
    return this.odata.getById(`/EmpresaClaseRisgos(Empresa_Id=${empresaId},Clase_Riesgo_Id=${claseRiesgoId})`, { expand });
  }

  updateEmpresaClaseRisgo(empresaId: number | null, claseRiesgoId: number | null, expand: string | null, empresaClaseRisgo: models.EmpresaClaseRisgo | null) : Observable<any> {
    return this.odata.patch(`/EmpresaClaseRisgos(Empresa_Id=${empresaId},Clase_Riesgo_Id=${claseRiesgoId})`, empresaClaseRisgo, item => item.Empresa_Id == empresaId && item.Clase_Riesgo_Id == claseRiesgoId, { expand }, ['Empresa','ClaseRiesgo']);
  }

  deleteEnfermedadesInfoDemofrafica(enfermedadId: number | null, infoDemograficaId: number | null) : Observable<any> {
    return this.odata.delete(`/EnfermedadesInfoDemofraficas(Enfermedad_Id=${enfermedadId},Info_Demografica_Id=${infoDemograficaId})`, item => !(item.Enfermedad_Id == enfermedadId && item.Info_Demografica_Id == infoDemograficaId));
  }

  getEnfermedadesInfoDemofraficaByEnfermedadIdAndInfoDemograficaId(enfermedadId: number | null, infoDemograficaId: number | null, expand: string | null) : Observable<any> {
    return this.odata.getById(`/EnfermedadesInfoDemofraficas(Enfermedad_Id=${enfermedadId},Info_Demografica_Id=${infoDemograficaId})`, { expand });
  }

  updateEnfermedadesInfoDemofrafica(enfermedadId: number | null, infoDemograficaId: number | null, expand: string | null, enfermedadesInfoDemofrafica: models.EnfermedadesInfoDemofrafica | null) : Observable<any> {
    return this.odata.patch(`/EnfermedadesInfoDemofraficas(Enfermedad_Id=${enfermedadId},Info_Demografica_Id=${infoDemograficaId})`, enfermedadesInfoDemofrafica, item => item.Enfermedad_Id == enfermedadId && item.Info_Demografica_Id == infoDemograficaId, { expand }, ['Enfermedade','InfoDemografica']);
  }
}
