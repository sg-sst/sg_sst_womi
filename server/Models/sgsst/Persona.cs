using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Personas", Schema = "public")]
  public partial class Persona
  {
    [ConcurrencyCheck]
    public Int64 TipoDocumento
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public int Documento
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Apellidos
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Email
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Telefono
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Direccion
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public DateTime Fecha_Nac
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public string Genero
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public Int64 Empresa_Id
    {
      get;
      set;
    }

    [ForeignKey("Empresa_Id")]
    public Empresa Empresa { get; set; }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("Persona")]
    public ICollection<InfoDemografica> InfoDemograficas { get; set; }
    [ConcurrencyCheck]
    public DateTime Fecha_Crea
    {
      get;
      set;
    }
  }
}
