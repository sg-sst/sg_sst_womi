using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Actividades_Personas", Schema = "public")]
  public partial class ActividadesPersona
  {
    [Key]
    public Int64 Actividad_Bienestar_Id
    {
      get;
      set;
    }

    [ForeignKey("Actividad_Bienestar_Id")]
    public ActividadesBienestar ActividadesBienestar { get; set; }
    public Int64 Info_Demografica_Id
    {
      get;
      set;
    }

    [ForeignKey("Info_Demografica_Id")]
    public InfoDemografica InfoDemografica { get; set; }
  }
}
