import { Component, Injector } from '@angular/core';
import { UbicacionsGenerated } from './ubicacions-generated.component';

@Component({
  selector: 'page-ubicacions',
  templateUrl: './ubicacions.component.html'
})
export class UbicacionsComponent extends UbicacionsGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
