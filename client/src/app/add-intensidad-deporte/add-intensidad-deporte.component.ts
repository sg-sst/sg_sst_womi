import { Component, Injector } from '@angular/core';
import { AddIntensidadDeporteGenerated } from './add-intensidad-deporte-generated.component';

@Component({
  selector: 'page-add-intensidad-deporte',
  templateUrl: './add-intensidad-deporte.component.html'
})
export class AddIntensidadDeporteComponent extends AddIntensidadDeporteGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
