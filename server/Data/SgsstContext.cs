using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

using Sgsst.Models.Sgsst;

namespace Sgsst.Data
{
    public partial class SgsstContext : Microsoft.EntityFrameworkCore.DbContext
    {
        private readonly IHttpContextAccessor httpAccessor;

        public SgsstContext(IHttpContextAccessor httpAccessor, DbContextOptions<SgsstContext> options):base(options)
        {
            this.httpAccessor = httpAccessor;
        }

        public SgsstContext(IHttpContextAccessor httpAccessor)
        {
            this.httpAccessor = httpAccessor;
        }

        partial void OnModelBuilding(ModelBuilder builder);

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Sgsst.Models.Sgsst.ActividadesPersona>().HasKey(table => new {
              table.Actividad_Bienestar_Id, table.Info_Demografica_Id
            });
            builder.Entity<Sgsst.Models.Sgsst.EmpresaClaseRisgo>().HasKey(table => new {
              table.Empresa_Id, table.Clase_Riesgo_Id
            });
            builder.Entity<Sgsst.Models.Sgsst.EnfermedadesInfoDemofrafica>().HasKey(table => new {
              table.Enfermedad_Id, table.Info_Demografica_Id
            });
            builder.Entity<Sgsst.Models.Sgsst.ActividadesPersona>()
                  .HasOne(i => i.ActividadesBienestar)
                  .WithMany(i => i.ActividadesPersonas)
                  .HasForeignKey(i => i.Actividad_Bienestar_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.ActividadesPersona>()
                  .HasOne(i => i.InfoDemografica)
                  .WithMany(i => i.ActividadesPersonas)
                  .HasForeignKey(i => i.Info_Demografica_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.Documento>()
                  .HasOne(i => i.TipoArchivo)
                  .WithMany(i => i.Documentos)
                  .HasForeignKey(i => i.Tipo_Archivo_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.Documento>()
                  .HasOne(i => i.Empresa)
                  .WithMany(i => i.Documentos)
                  .HasForeignKey(i => i.Empresa_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.Empresa>()
                  .HasOne(i => i.ActividadEconomica)
                  .WithMany(i => i.Empresas)
                  .HasForeignKey(i => i.Actividad_Economica_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.Empresa>()
                  .HasOne(i => i.Ubicacion)
                  .WithMany(i => i.Empresas)
                  .HasForeignKey(i => i.Ubicacion_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.Empresa>()
                  .HasOne(i => i.EstadoEmpresa)
                  .WithMany(i => i.Empresas)
                  .HasForeignKey(i => i.Estado_Empresa_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.EmpresaClaseRisgo>()
                  .HasOne(i => i.Empresa)
                  .WithMany(i => i.EmpresaClaseRisgos)
                  .HasForeignKey(i => i.Empresa_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.EmpresaClaseRisgo>()
                  .HasOne(i => i.ClaseRiesgo)
                  .WithMany(i => i.EmpresaClaseRisgos)
                  .HasForeignKey(i => i.Clase_Riesgo_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.EnfermedadesInfoDemofrafica>()
                  .HasOne(i => i.Enfermedade)
                  .WithMany(i => i.EnfermedadesInfoDemofraficas)
                  .HasForeignKey(i => i.Enfermedad_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.EnfermedadesInfoDemofrafica>()
                  .HasOne(i => i.InfoDemografica)
                  .WithMany(i => i.EnfermedadesInfoDemofraficas)
                  .HasForeignKey(i => i.Info_Demografica_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .HasOne(i => i.Persona)
                  .WithMany(i => i.InfoDemograficas)
                  .HasForeignKey(i => i.Persona_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .HasOne(i => i.ClaseRiesgo)
                  .WithMany(i => i.InfoDemograficas)
                  .HasForeignKey(i => i.Clase_Riesgo_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .HasOne(i => i.EstadoCivil)
                  .WithMany(i => i.InfoDemograficas)
                  .HasForeignKey(i => i.Estado_Civil_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .HasOne(i => i.TiempoLibre)
                  .WithMany(i => i.InfoDemograficas)
                  .HasForeignKey(i => i.Tiempo_Libre_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .HasOne(i => i.PromedioIngreso)
                  .WithMany(i => i.InfoDemograficas)
                  .HasForeignKey(i => i.Promedio_Ingresos_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .HasOne(i => i.RangoIngreso)
                  .WithMany(i => i.InfoDemograficas)
                  .HasForeignKey(i => i.Rango_Ingresos_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .HasOne(i => i.TipoContrato)
                  .WithMany(i => i.InfoDemograficas)
                  .HasForeignKey(i => i.Tipo_Contrato_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .HasOne(i => i.IntensidadDeporte)
                  .WithMany(i => i.InfoDemograficas)
                  .HasForeignKey(i => i.Intensidad_Deporte_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.Persona>()
                  .HasOne(i => i.Empresa)
                  .WithMany(i => i.Personas)
                  .HasForeignKey(i => i.Empresa_Id)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<Sgsst.Models.Sgsst.Ubicacion>()
                  .HasOne(i => i.Ubicacion1)
                  .WithMany(i => i.Ubicacions1)
                  .HasForeignKey(i => i.Padre_Id)
                  .HasPrincipalKey(i => i.Id);

            builder.Entity<Sgsst.Models.Sgsst.ActividadEconomica>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Actividad_Economica_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.ActividadesBienestar>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Actividades_Bienestar_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.ClaseRiesgo>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Clase_Riesgo_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.Documento>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Documentos_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.Documento>()
                  .Property(p => p.Fecha_Crea)
                  .HasDefaultValueSql("now()");

            builder.Entity<Sgsst.Models.Sgsst.Empresa>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Empresa_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.Enfermedade>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Enfermedades_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.EstadoCivil>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Estado_Civil_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.EstadoEmpresa>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Estado_Empresas_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Info_Demografica_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.InfoDemografica>()
                  .Property(p => p.Fecha_Crea)
                  .HasDefaultValueSql("now()");

            builder.Entity<Sgsst.Models.Sgsst.IntensidadDeporte>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Intensidad_Deporte_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.Persona>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Personas_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.Persona>()
                  .Property(p => p.Fecha_Crea)
                  .HasDefaultValueSql("now()");

            builder.Entity<Sgsst.Models.Sgsst.PromedioIngreso>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Promedio_Ingresos_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.RangoIngreso>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Rango_Ingresos_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.TiempoLibre>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Tiempo_Libre_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.TipoArchivo>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Tipo_Archivo_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.TipoContrato>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Tipo_Contrato_seq\"'::regclass)");

            builder.Entity<Sgsst.Models.Sgsst.Ubicacion>()
                  .Property(p => p.Id)
                  .HasDefaultValueSql("nextval('\"Ubicacion_seq\"'::regclass)");


            this.OnModelBuilding(builder);
        }


        public DbSet<Sgsst.Models.Sgsst.ActividadEconomica> ActividadEconomicas
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.ActividadesBienestar> ActividadesBienestars
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.ActividadesPersona> ActividadesPersonas
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.ClaseRiesgo> ClaseRiesgos
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.Documento> Documentos
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.Empresa> Empresas
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.EmpresaClaseRisgo> EmpresaClaseRisgos
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.Enfermedade> Enfermedades
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.EnfermedadesInfoDemofrafica> EnfermedadesInfoDemofraficas
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.EstadoCivil> EstadoCivils
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.EstadoEmpresa> EstadoEmpresas
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.InfoDemografica> InfoDemograficas
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.IntensidadDeporte> IntensidadDeportes
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.Persona> Personas
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.PromedioIngreso> PromedioIngresos
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.RangoIngreso> RangoIngresos
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.TiempoLibre> TiempoLibres
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.TipoArchivo> TipoArchivos
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.TipoContrato> TipoContratos
        {
          get;
          set;
        }

        public DbSet<Sgsst.Models.Sgsst.Ubicacion> Ubicacions
        {
          get;
          set;
        }
    }
}
