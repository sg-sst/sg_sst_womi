import { Component, Injector } from '@angular/core';
import { IntensidadDeportesGenerated } from './intensidad-deportes-generated.component';

@Component({
  selector: 'page-intensidad-deportes',
  templateUrl: './intensidad-deportes.component.html'
})
export class IntensidadDeportesComponent extends IntensidadDeportesGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
