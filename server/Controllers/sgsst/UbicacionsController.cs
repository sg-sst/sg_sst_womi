using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/Ubicacions")]
  [Route("mvc/odata/sgsst/Ubicacions")]
  public partial class UbicacionsController : ODataController
  {
    private Data.SgsstContext context;

    public UbicacionsController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/Ubicacions
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.Ubicacion> GetUbicacions()
    {
      var items = this.context.Ubicacions.AsQueryable<Models.Sgsst.Ubicacion>();
      this.OnUbicacionsRead(ref items);

      return items;
    }

    partial void OnUbicacionsRead(ref IQueryable<Models.Sgsst.Ubicacion> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<Ubicacion> GetUbicacion(Int64 key)
    {
        var items = this.context.Ubicacions.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnUbicacionDeleted(Models.Sgsst.Ubicacion item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteUbicacion(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.Ubicacions
                .Where(i => i.Id == key)
                .Include(i => i.Empresas)
                .Include(i => i.Ubicacions1)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Ubicacion>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnUbicacionDeleted(item);
            this.context.Ubicacions.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnUbicacionUpdated(Models.Sgsst.Ubicacion item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutUbicacion(Int64 key, [FromBody]Models.Sgsst.Ubicacion newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Ubicacions
                .Where(i => i.Id == key)
                .Include(i => i.Empresas)
                .Include(i => i.Ubicacions1)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Ubicacion>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnUbicacionUpdated(newItem);
            this.context.Ubicacions.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.Ubicacions.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "Ubicacion1");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchUbicacion(Int64 key, [FromBody]Delta<Models.Sgsst.Ubicacion> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Ubicacions.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.Ubicacion>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnUbicacionUpdated(item);
            this.context.Ubicacions.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.Ubicacions.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "Ubicacion1");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnUbicacionCreated(Models.Sgsst.Ubicacion item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.Ubicacion item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnUbicacionCreated(item);
            this.context.Ubicacions.Add(item);
            this.context.SaveChanges();

            var key = item.Id;

            var itemToReturn = this.context.Ubicacions.Where(i => i.Id == key);

            Request.QueryString = Request.QueryString.Add("$expand", "Ubicacion1");

            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
