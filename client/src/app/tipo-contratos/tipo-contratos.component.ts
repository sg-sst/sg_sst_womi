import { Component, Injector } from '@angular/core';
import { TipoContratosGenerated } from './tipo-contratos-generated.component';

@Component({
  selector: 'page-tipo-contratos',
  templateUrl: './tipo-contratos.component.html'
})
export class TipoContratosComponent extends TipoContratosGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
