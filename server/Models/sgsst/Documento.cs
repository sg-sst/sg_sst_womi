using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Documentos", Schema = "public")]
  public partial class Documento
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public Int64? Tipo_Archivo_Id
    {
      get;
      set;
    }

    [ForeignKey("Tipo_Archivo_Id")]
    public TipoArchivo TipoArchivo { get; set; }
    [ConcurrencyCheck]
    public Int64? Empresa_Id
    {
      get;
      set;
    }

    [ForeignKey("Empresa_Id")]
    public Empresa Empresa { get; set; }
    [ConcurrencyCheck]
    public string Ruta
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public DateTime? Fecha_Mod
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public DateTime Fecha_Crea
    {
      get;
      set;
    }
  }
}
