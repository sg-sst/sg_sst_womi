using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/EmpresaClaseRisgos")]
  [Route("mvc/odata/sgsst/EmpresaClaseRisgos")]
  public partial class EmpresaClaseRisgosController : ODataController
  {
    private Data.SgsstContext context;

    public EmpresaClaseRisgosController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/EmpresaClaseRisgos
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.EmpresaClaseRisgo> GetEmpresaClaseRisgos()
    {
      var items = this.context.EmpresaClaseRisgos.AsQueryable<Models.Sgsst.EmpresaClaseRisgo>();
      this.OnEmpresaClaseRisgosRead(ref items);

      return items;
    }

    partial void OnEmpresaClaseRisgosRead(ref IQueryable<Models.Sgsst.EmpresaClaseRisgo> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Empresa_Id},{Clase_Riesgo_Id}")]
    public SingleResult<EmpresaClaseRisgo> GetEmpresaClaseRisgo([FromODataUri] Int64 keyEmpresa_Id,[FromODataUri] Int64 keyClase_Riesgo_Id)
    {
        var items = this.context.EmpresaClaseRisgos.Where(i=>i.Empresa_Id == keyEmpresa_Id && i.Clase_Riesgo_Id == keyClase_Riesgo_Id);
        return SingleResult.Create(items);
    }
    partial void OnEmpresaClaseRisgoDeleted(Models.Sgsst.EmpresaClaseRisgo item);

    [HttpDelete("{Empresa_Id},{Clase_Riesgo_Id}")]
    public IActionResult DeleteEmpresaClaseRisgo([FromODataUri] Int64 keyEmpresa_Id,[FromODataUri] Int64 keyClase_Riesgo_Id)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.EmpresaClaseRisgos
                .Where(i => i.Empresa_Id == keyEmpresa_Id && i.Clase_Riesgo_Id == keyClase_Riesgo_Id)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.EmpresaClaseRisgo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEmpresaClaseRisgoDeleted(item);
            this.context.EmpresaClaseRisgos.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEmpresaClaseRisgoUpdated(Models.Sgsst.EmpresaClaseRisgo item);

    [HttpPut("{Empresa_Id},{Clase_Riesgo_Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutEmpresaClaseRisgo([FromODataUri] Int64 keyEmpresa_Id,[FromODataUri] Int64 keyClase_Riesgo_Id, [FromBody]Models.Sgsst.EmpresaClaseRisgo newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.EmpresaClaseRisgos
                .Where(i => i.Empresa_Id == keyEmpresa_Id && i.Clase_Riesgo_Id == keyClase_Riesgo_Id)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.EmpresaClaseRisgo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEmpresaClaseRisgoUpdated(newItem);
            this.context.EmpresaClaseRisgos.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.EmpresaClaseRisgos.Where(i => i.Empresa_Id == keyEmpresa_Id && i.Clase_Riesgo_Id == keyClase_Riesgo_Id);
            Request.QueryString = Request.QueryString.Add("$expand", "Empresa,ClaseRiesgo");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Empresa_Id},{Clase_Riesgo_Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchEmpresaClaseRisgo([FromODataUri] Int64 keyEmpresa_Id,[FromODataUri] Int64 keyClase_Riesgo_Id, [FromBody]Delta<Models.Sgsst.EmpresaClaseRisgo> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.EmpresaClaseRisgos.Where(i => i.Empresa_Id == keyEmpresa_Id && i.Clase_Riesgo_Id == keyClase_Riesgo_Id);

            items = EntityPatch.ApplyTo<Models.Sgsst.EmpresaClaseRisgo>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnEmpresaClaseRisgoUpdated(item);
            this.context.EmpresaClaseRisgos.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.EmpresaClaseRisgos.Where(i => i.Empresa_Id == keyEmpresa_Id && i.Clase_Riesgo_Id == keyClase_Riesgo_Id);
            Request.QueryString = Request.QueryString.Add("$expand", "Empresa,ClaseRiesgo");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEmpresaClaseRisgoCreated(Models.Sgsst.EmpresaClaseRisgo item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.EmpresaClaseRisgo item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnEmpresaClaseRisgoCreated(item);
            this.context.EmpresaClaseRisgos.Add(item);
            this.context.SaveChanges();

            var keyEmpresa_Id = item.Empresa_Id;
            var keyClase_Riesgo_Id = item.Clase_Riesgo_Id;

            var itemToReturn = this.context.EmpresaClaseRisgos.Where(i => i.Empresa_Id == keyEmpresa_Id && i.Clase_Riesgo_Id == keyClase_Riesgo_Id);

            Request.QueryString = Request.QueryString.Add("$expand", "Empresa,ClaseRiesgo");

            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
