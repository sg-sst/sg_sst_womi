import { Component, Injector } from '@angular/core';
import { AddClaseRiesgoGenerated } from './add-clase-riesgo-generated.component';

@Component({
  selector: 'page-add-clase-riesgo',
  templateUrl: './add-clase-riesgo.component.html'
})
export class AddClaseRiesgoComponent extends AddClaseRiesgoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
