import { Component, Injector } from '@angular/core';
import { ActividadesPersonasGenerated } from './actividades-personas-generated.component';

@Component({
  selector: 'page-actividades-personas',
  templateUrl: './actividades-personas.component.html'
})
export class ActividadesPersonasComponent extends ActividadesPersonasGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
