import { Component, Injector } from '@angular/core';
import { PersonasGenerated } from './personas-generated.component';

@Component({
  selector: 'page-personas',
  templateUrl: './personas.component.html'
})
export class PersonasComponent extends PersonasGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
