/*
  This file is automatically generated. Any changes will be overwritten.
  Modify app.module.ts instead.
*/
import { APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BodyModule } from '@radzen/angular/dist/body';
import { CardModule } from '@radzen/angular/dist/card';
import { ContentContainerModule } from '@radzen/angular/dist/content-container';
import { HeaderModule } from '@radzen/angular/dist/header';
import { SidebarToggleModule } from '@radzen/angular/dist/sidebar-toggle';
import { LabelModule } from '@radzen/angular/dist/label';
import { ProfileMenuModule } from '@radzen/angular/dist/profilemenu';
import { GravatarModule } from '@radzen/angular/dist/gravatar';
import { SidebarModule } from '@radzen/angular/dist/sidebar';
import { PanelMenuModule } from '@radzen/angular/dist/panelmenu';
import { FooterModule } from '@radzen/angular/dist/footer';
import { ContentModule } from '@radzen/angular/dist/content';
import { HeadingModule } from '@radzen/angular/dist/heading';
import { GridModule } from '@radzen/angular/dist/grid';
import { FormModule } from '@radzen/angular/dist/form';
import { ButtonModule } from '@radzen/angular/dist/button';
import { LoginModule } from '@radzen/angular/dist/login';
import { HtmlModule } from '@radzen/angular/dist/html';
import { SharedModule } from '@radzen/angular/dist/shared';
import { NotificationModule } from '@radzen/angular/dist/notification';
import { DialogModule } from '@radzen/angular/dist/dialog';

import { ConfigService, configServiceFactory } from './config.service';
import { AppRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { CacheInterceptor } from './cache.interceptor';
export { AppComponent } from './app.component';
import { ActividadEconomicasComponent } from './actividad-economicas/actividad-economicas.component';
import { AddActividadEconomicaComponent } from './add-actividad-economica/add-actividad-economica.component';
import { EditActividadEconomicaComponent } from './edit-actividad-economica/edit-actividad-economica.component';
import { ActividadesBienestarsComponent } from './actividades-bienestars/actividades-bienestars.component';
import { AddActividadesBienestarComponent } from './add-actividades-bienestar/add-actividades-bienestar.component';
import { EditActividadesBienestarComponent } from './edit-actividades-bienestar/edit-actividades-bienestar.component';
import { ActividadesPersonasComponent } from './actividades-personas/actividades-personas.component';
import { AddActividadesPersonaComponent } from './add-actividades-persona/add-actividades-persona.component';
import { EditActividadesPersonaComponent } from './edit-actividades-persona/edit-actividades-persona.component';
import { ClaseRiesgosComponent } from './clase-riesgos/clase-riesgos.component';
import { AddClaseRiesgoComponent } from './add-clase-riesgo/add-clase-riesgo.component';
import { EditClaseRiesgoComponent } from './edit-clase-riesgo/edit-clase-riesgo.component';
import { DocumentosComponent } from './documentos/documentos.component';
import { AddDocumentoComponent } from './add-documento/add-documento.component';
import { EditDocumentoComponent } from './edit-documento/edit-documento.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { AddEmpresaComponent } from './add-empresa/add-empresa.component';
import { EditEmpresaComponent } from './edit-empresa/edit-empresa.component';
import { EmpresaClaseRisgosComponent } from './empresa-clase-risgos/empresa-clase-risgos.component';
import { AddEmpresaClaseRisgoComponent } from './add-empresa-clase-risgo/add-empresa-clase-risgo.component';
import { EditEmpresaClaseRisgoComponent } from './edit-empresa-clase-risgo/edit-empresa-clase-risgo.component';
import { EnfermedadesComponent } from './enfermedades/enfermedades.component';
import { AddEnfermedadeComponent } from './add-enfermedade/add-enfermedade.component';
import { EditEnfermedadeComponent } from './edit-enfermedade/edit-enfermedade.component';
import { EnfermedadesInfoDemofraficasComponent } from './enfermedades-info-demofraficas/enfermedades-info-demofraficas.component';
import { AddEnfermedadesInfoDemofraficaComponent } from './add-enfermedades-info-demofrafica/add-enfermedades-info-demofrafica.component';
import { EditEnfermedadesInfoDemofraficaComponent } from './edit-enfermedades-info-demofrafica/edit-enfermedades-info-demofrafica.component';
import { EstadoCivilsComponent } from './estado-civils/estado-civils.component';
import { AddEstadoCivilComponent } from './add-estado-civil/add-estado-civil.component';
import { EditEstadoCivilComponent } from './edit-estado-civil/edit-estado-civil.component';
import { EstadoEmpresasComponent } from './estado-empresas/estado-empresas.component';
import { AddEstadoEmpresaComponent } from './add-estado-empresa/add-estado-empresa.component';
import { EditEstadoEmpresaComponent } from './edit-estado-empresa/edit-estado-empresa.component';
import { InfoDemograficasComponent } from './info-demograficas/info-demograficas.component';
import { AddInfoDemograficaComponent } from './add-info-demografica/add-info-demografica.component';
import { EditInfoDemograficaComponent } from './edit-info-demografica/edit-info-demografica.component';
import { IntensidadDeportesComponent } from './intensidad-deportes/intensidad-deportes.component';
import { AddIntensidadDeporteComponent } from './add-intensidad-deporte/add-intensidad-deporte.component';
import { EditIntensidadDeporteComponent } from './edit-intensidad-deporte/edit-intensidad-deporte.component';
import { PersonasComponent } from './personas/personas.component';
import { AddPersonaComponent } from './add-persona/add-persona.component';
import { EditPersonaComponent } from './edit-persona/edit-persona.component';
import { PromedioIngresosComponent } from './promedio-ingresos/promedio-ingresos.component';
import { AddPromedioIngresoComponent } from './add-promedio-ingreso/add-promedio-ingreso.component';
import { EditPromedioIngresoComponent } from './edit-promedio-ingreso/edit-promedio-ingreso.component';
import { RangoIngresosComponent } from './rango-ingresos/rango-ingresos.component';
import { AddRangoIngresoComponent } from './add-rango-ingreso/add-rango-ingreso.component';
import { EditRangoIngresoComponent } from './edit-rango-ingreso/edit-rango-ingreso.component';
import { TiempoLibresComponent } from './tiempo-libres/tiempo-libres.component';
import { AddTiempoLibreComponent } from './add-tiempo-libre/add-tiempo-libre.component';
import { EditTiempoLibreComponent } from './edit-tiempo-libre/edit-tiempo-libre.component';
import { TipoArchivosComponent } from './tipo-archivos/tipo-archivos.component';
import { AddTipoArchivoComponent } from './add-tipo-archivo/add-tipo-archivo.component';
import { EditTipoArchivoComponent } from './edit-tipo-archivo/edit-tipo-archivo.component';
import { TipoContratosComponent } from './tipo-contratos/tipo-contratos.component';
import { AddTipoContratoComponent } from './add-tipo-contrato/add-tipo-contrato.component';
import { EditTipoContratoComponent } from './edit-tipo-contrato/edit-tipo-contrato.component';
import { UbicacionsComponent } from './ubicacions/ubicacions.component';
import { AddUbicacionComponent } from './add-ubicacion/add-ubicacion.component';
import { EditUbicacionComponent } from './edit-ubicacion/edit-ubicacion.component';
import { LoginComponent } from './login/login.component';
import { ApplicationUsersComponent } from './application-users/application-users.component';
import { ApplicationRolesComponent } from './application-roles/application-roles.component';
import { RegisterApplicationUserComponent } from './register-application-user/register-application-user.component';
import { AddApplicationRoleComponent } from './add-application-role/add-application-role.component';
import { AddApplicationUserComponent } from './add-application-user/add-application-user.component';
import { EditApplicationUserComponent } from './edit-application-user/edit-application-user.component';
import { ProfileComponent } from './profile/profile.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';

import { SgsstService } from './sgsst.service';
import { SecurityService, UserService } from './security.service';
import { SecurityInterceptor } from './security.interceptor';
import { AuthGuard } from './auth.guard';

export const PageDeclarations = [
  ActividadEconomicasComponent,
  AddActividadEconomicaComponent,
  EditActividadEconomicaComponent,
  ActividadesBienestarsComponent,
  AddActividadesBienestarComponent,
  EditActividadesBienestarComponent,
  ActividadesPersonasComponent,
  AddActividadesPersonaComponent,
  EditActividadesPersonaComponent,
  ClaseRiesgosComponent,
  AddClaseRiesgoComponent,
  EditClaseRiesgoComponent,
  DocumentosComponent,
  AddDocumentoComponent,
  EditDocumentoComponent,
  EmpresasComponent,
  AddEmpresaComponent,
  EditEmpresaComponent,
  EmpresaClaseRisgosComponent,
  AddEmpresaClaseRisgoComponent,
  EditEmpresaClaseRisgoComponent,
  EnfermedadesComponent,
  AddEnfermedadeComponent,
  EditEnfermedadeComponent,
  EnfermedadesInfoDemofraficasComponent,
  AddEnfermedadesInfoDemofraficaComponent,
  EditEnfermedadesInfoDemofraficaComponent,
  EstadoCivilsComponent,
  AddEstadoCivilComponent,
  EditEstadoCivilComponent,
  EstadoEmpresasComponent,
  AddEstadoEmpresaComponent,
  EditEstadoEmpresaComponent,
  InfoDemograficasComponent,
  AddInfoDemograficaComponent,
  EditInfoDemograficaComponent,
  IntensidadDeportesComponent,
  AddIntensidadDeporteComponent,
  EditIntensidadDeporteComponent,
  PersonasComponent,
  AddPersonaComponent,
  EditPersonaComponent,
  PromedioIngresosComponent,
  AddPromedioIngresoComponent,
  EditPromedioIngresoComponent,
  RangoIngresosComponent,
  AddRangoIngresoComponent,
  EditRangoIngresoComponent,
  TiempoLibresComponent,
  AddTiempoLibreComponent,
  EditTiempoLibreComponent,
  TipoArchivosComponent,
  AddTipoArchivoComponent,
  EditTipoArchivoComponent,
  TipoContratosComponent,
  AddTipoContratoComponent,
  EditTipoContratoComponent,
  UbicacionsComponent,
  AddUbicacionComponent,
  EditUbicacionComponent,
  LoginComponent,
  ApplicationUsersComponent,
  ApplicationRolesComponent,
  RegisterApplicationUserComponent,
  AddApplicationRoleComponent,
  AddApplicationUserComponent,
  EditApplicationUserComponent,
  ProfileComponent,
  UnauthorizedComponent,
];

export const LayoutDeclarations = [
  LoginLayoutComponent,
  MainLayoutComponent,
];

export const AppDeclarations = [
  ...PageDeclarations,
  ...LayoutDeclarations,
  AppComponent
];

export const AppProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: CacheInterceptor,
    multi: true
  },
  SgsstService,
  UserService,
  SecurityService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: SecurityInterceptor,
    multi: true
  },
  AuthGuard,
  ConfigService,
  {
    provide: APP_INITIALIZER,
    useFactory: configServiceFactory,
    deps: [ConfigService],
    multi: true
  }
];

export const AppImports = [
  BrowserModule,
  BrowserAnimationsModule,
  FormsModule,
  HttpClientModule,
  BodyModule,
  CardModule,
  ContentContainerModule,
  HeaderModule,
  SidebarToggleModule,
  LabelModule,
  ProfileMenuModule,
  GravatarModule,
  SidebarModule,
  PanelMenuModule,
  FooterModule,
  ContentModule,
  HeadingModule,
  GridModule,
  FormModule,
  ButtonModule,
  LoginModule,
  HtmlModule,
  SharedModule,
  NotificationModule,
  DialogModule,
  AppRoutes
];
