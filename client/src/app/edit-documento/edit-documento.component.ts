import { Component, Injector } from '@angular/core';
import { EditDocumentoGenerated } from './edit-documento-generated.component';

@Component({
  selector: 'page-edit-documento',
  templateUrl: './edit-documento.component.html'
})
export class EditDocumentoComponent extends EditDocumentoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
