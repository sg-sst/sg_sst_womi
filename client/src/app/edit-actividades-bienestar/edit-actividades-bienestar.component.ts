import { Component, Injector } from '@angular/core';
import { EditActividadesBienestarGenerated } from './edit-actividades-bienestar-generated.component';

@Component({
  selector: 'page-edit-actividades-bienestar',
  templateUrl: './edit-actividades-bienestar.component.html'
})
export class EditActividadesBienestarComponent extends EditActividadesBienestarGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
