using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Empresa_Clase_Risgo", Schema = "public")]
  public partial class EmpresaClaseRisgo
  {
    [Key]
    public Int64 Empresa_Id
    {
      get;
      set;
    }

    [ForeignKey("Empresa_Id")]
    public Empresa Empresa { get; set; }
    public Int64 Clase_Riesgo_Id
    {
      get;
      set;
    }

    [ForeignKey("Clase_Riesgo_Id")]
    public ClaseRiesgo ClaseRiesgo { get; set; }
  }
}
