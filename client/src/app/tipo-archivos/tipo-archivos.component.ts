import { Component, Injector } from '@angular/core';
import { TipoArchivosGenerated } from './tipo-archivos-generated.component';

@Component({
  selector: 'page-tipo-archivos',
  templateUrl: './tipo-archivos.component.html'
})
export class TipoArchivosComponent extends TipoArchivosGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
