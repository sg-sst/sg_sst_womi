using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/RangoIngresos")]
  [Route("mvc/odata/sgsst/RangoIngresos")]
  public partial class RangoIngresosController : ODataController
  {
    private Data.SgsstContext context;

    public RangoIngresosController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/RangoIngresos
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.RangoIngreso> GetRangoIngresos()
    {
      var items = this.context.RangoIngresos.AsQueryable<Models.Sgsst.RangoIngreso>();
      this.OnRangoIngresosRead(ref items);

      return items;
    }

    partial void OnRangoIngresosRead(ref IQueryable<Models.Sgsst.RangoIngreso> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<RangoIngreso> GetRangoIngreso(Int64 key)
    {
        var items = this.context.RangoIngresos.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnRangoIngresoDeleted(Models.Sgsst.RangoIngreso item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteRangoIngreso(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.RangoIngresos
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.RangoIngreso>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnRangoIngresoDeleted(item);
            this.context.RangoIngresos.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnRangoIngresoUpdated(Models.Sgsst.RangoIngreso item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutRangoIngreso(Int64 key, [FromBody]Models.Sgsst.RangoIngreso newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.RangoIngresos
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.RangoIngreso>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnRangoIngresoUpdated(newItem);
            this.context.RangoIngresos.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.RangoIngresos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchRangoIngreso(Int64 key, [FromBody]Delta<Models.Sgsst.RangoIngreso> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.RangoIngresos.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.RangoIngreso>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnRangoIngresoUpdated(item);
            this.context.RangoIngresos.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.RangoIngresos.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnRangoIngresoCreated(Models.Sgsst.RangoIngreso item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.RangoIngreso item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnRangoIngresoCreated(item);
            this.context.RangoIngresos.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/RangoIngresos/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
