import { Component, Injector } from '@angular/core';
import { EditInfoDemograficaGenerated } from './edit-info-demografica-generated.component';

@Component({
  selector: 'page-edit-info-demografica',
  templateUrl: './edit-info-demografica.component.html'
})
export class EditInfoDemograficaComponent extends EditInfoDemograficaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
