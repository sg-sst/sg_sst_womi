using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/Personas")]
  [Route("mvc/odata/sgsst/Personas")]
  public partial class PersonasController : ODataController
  {
    private Data.SgsstContext context;

    public PersonasController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/Personas
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.Persona> GetPersonas()
    {
      var items = this.context.Personas.AsQueryable<Models.Sgsst.Persona>();
      this.OnPersonasRead(ref items);

      return items;
    }

    partial void OnPersonasRead(ref IQueryable<Models.Sgsst.Persona> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<Persona> GetPersona(Int64 key)
    {
        var items = this.context.Personas.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnPersonaDeleted(Models.Sgsst.Persona item);

    [HttpDelete("{Id}")]
    public IActionResult DeletePersona(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.Personas
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Persona>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnPersonaDeleted(item);
            this.context.Personas.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnPersonaUpdated(Models.Sgsst.Persona item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutPersona(Int64 key, [FromBody]Models.Sgsst.Persona newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Personas
                .Where(i => i.Id == key)
                .Include(i => i.InfoDemograficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Persona>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnPersonaUpdated(newItem);
            this.context.Personas.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.Personas.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchPersona(Int64 key, [FromBody]Delta<Models.Sgsst.Persona> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Personas.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.Persona>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnPersonaUpdated(item);
            this.context.Personas.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.Personas.Where(i => i.Id == key);
            Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnPersonaCreated(Models.Sgsst.Persona item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.Persona item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnPersonaCreated(item);
            this.context.Personas.Add(item);
            this.context.SaveChanges();

            var key = item.Id;

            var itemToReturn = this.context.Personas.Where(i => i.Id == key);

            Request.QueryString = Request.QueryString.Add("$expand", "Empresa");

            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
