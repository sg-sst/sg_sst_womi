import { Component, Injector } from '@angular/core';
import { AddTipoArchivoGenerated } from './add-tipo-archivo-generated.component';

@Component({
  selector: 'page-add-tipo-archivo',
  templateUrl: './add-tipo-archivo.component.html'
})
export class AddTipoArchivoComponent extends AddTipoArchivoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
