import { Component, Injector } from '@angular/core';
import { AddTipoContratoGenerated } from './add-tipo-contrato-generated.component';

@Component({
  selector: 'page-add-tipo-contrato',
  templateUrl: './add-tipo-contrato.component.html'
})
export class AddTipoContratoComponent extends AddTipoContratoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
