import { Component, Injector } from '@angular/core';
import { EditEstadoEmpresaGenerated } from './edit-estado-empresa-generated.component';

@Component({
  selector: 'page-edit-estado-empresa',
  templateUrl: './edit-estado-empresa.component.html'
})
export class EditEstadoEmpresaComponent extends EditEstadoEmpresaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
