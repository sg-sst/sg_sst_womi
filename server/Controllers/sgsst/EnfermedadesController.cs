using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/Enfermedades")]
  [Route("mvc/odata/sgsst/Enfermedades")]
  public partial class EnfermedadesController : ODataController
  {
    private Data.SgsstContext context;

    public EnfermedadesController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/Enfermedades
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.Enfermedade> GetEnfermedades()
    {
      var items = this.context.Enfermedades.AsQueryable<Models.Sgsst.Enfermedade>();
      this.OnEnfermedadesRead(ref items);

      return items;
    }

    partial void OnEnfermedadesRead(ref IQueryable<Models.Sgsst.Enfermedade> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<Enfermedade> GetEnfermedade(Int64 key)
    {
        var items = this.context.Enfermedades.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnEnfermedadeDeleted(Models.Sgsst.Enfermedade item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteEnfermedade(Int64 key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.Enfermedades
                .Where(i => i.Id == key)
                .Include(i => i.EnfermedadesInfoDemofraficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Enfermedade>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEnfermedadeDeleted(item);
            this.context.Enfermedades.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEnfermedadeUpdated(Models.Sgsst.Enfermedade item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutEnfermedade(Int64 key, [FromBody]Models.Sgsst.Enfermedade newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Enfermedades
                .Where(i => i.Id == key)
                .Include(i => i.EnfermedadesInfoDemofraficas)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.Enfermedade>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEnfermedadeUpdated(newItem);
            this.context.Enfermedades.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.Enfermedades.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchEnfermedade(Int64 key, [FromBody]Delta<Models.Sgsst.Enfermedade> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Enfermedades.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<Models.Sgsst.Enfermedade>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnEnfermedadeUpdated(item);
            this.context.Enfermedades.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.Enfermedades.Where(i => i.Id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEnfermedadeCreated(Models.Sgsst.Enfermedade item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.Enfermedade item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnEnfermedadeCreated(item);
            this.context.Enfermedades.Add(item);
            this.context.SaveChanges();

            return Created($"odata/Sgsst/Enfermedades/{item.Id}", item);
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
