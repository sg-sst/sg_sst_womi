export const environment = {
  serverMethodsUrl: 'http://localhost:5000/',
  sgsst: 'https://localhost:5000/odata/sgsst',

  securityUrl: 'https://localhost:5000/auth',
  production: true
};
