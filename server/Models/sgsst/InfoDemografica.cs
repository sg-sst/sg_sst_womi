using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Info_Demografica", Schema = "public")]
  public partial class InfoDemografica
  {
    [ConcurrencyCheck]
    public Int64 Persona_Id
    {
      get;
      set;
    }

    [ForeignKey("Persona_Id")]
    public Persona Persona { get; set; }
    [ConcurrencyCheck]
    public string Cargo
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public DateTime? Fecha_Ini_Cargo
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public Int64 Clase_Riesgo_Id
    {
      get;
      set;
    }

    [ForeignKey("Clase_Riesgo_Id")]
    public ClaseRiesgo ClaseRiesgo { get; set; }
    [ConcurrencyCheck]
    public Int64? Estado_Civil_Id
    {
      get;
      set;
    }

    [ForeignKey("Estado_Civil_Id")]
    public EstadoCivil EstadoCivil { get; set; }
    [ConcurrencyCheck]
    public bool? Tiene_Vivienda
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public int? Num_Persona
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public Int64 Tiempo_Libre_Id
    {
      get;
      set;
    }

    [ForeignKey("Tiempo_Libre_Id")]
    public TiempoLibre TiempoLibre { get; set; }
    [ConcurrencyCheck]
    public Int64? Promedio_Ingresos_Id
    {
      get;
      set;
    }

    [ForeignKey("Promedio_Ingresos_Id")]
    public PromedioIngreso PromedioIngreso { get; set; }
    [ConcurrencyCheck]
    public Int64? Rango_Ingresos_Id
    {
      get;
      set;
    }

    [ForeignKey("Rango_Ingresos_Id")]
    public RangoIngreso RangoIngreso { get; set; }
    [ConcurrencyCheck]
    public Int64? Tipo_Contrato_Id
    {
      get;
      set;
    }

    [ForeignKey("Tipo_Contrato_Id")]
    public TipoContrato TipoContrato { get; set; }
    [ConcurrencyCheck]
    public bool? Tiene_Enfermedad
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public bool? Fuma
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public decimal? Promedio_Fuma
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public bool? Practica_Deporte
    {
      get;
      set;
    }
    [ConcurrencyCheck]
    public Int64? Intensidad_Deporte_Id
    {
      get;
      set;
    }

    [ForeignKey("Intensidad_Deporte_Id")]
    public IntensidadDeporte IntensidadDeporte { get; set; }
    [ConcurrencyCheck]
    public bool? Consentimiento
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("InfoDemografica")]
    public ICollection<EnfermedadesInfoDemofrafica> EnfermedadesInfoDemofraficas { get; set; }

    [InverseProperty("InfoDemografica")]
    public ICollection<ActividadesPersona> ActividadesPersonas { get; set; }
    [ConcurrencyCheck]
    public DateTime Fecha_Crea
    {
      get;
      set;
    }
  }
}
