import { Component, Injector } from '@angular/core';
import { DocumentosGenerated } from './documentos-generated.component';

@Component({
  selector: 'page-documentos',
  templateUrl: './documentos.component.html'
})
export class DocumentosComponent extends DocumentosGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
