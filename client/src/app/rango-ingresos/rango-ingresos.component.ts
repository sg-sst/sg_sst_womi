import { Component, Injector } from '@angular/core';
import { RangoIngresosGenerated } from './rango-ingresos-generated.component';

@Component({
  selector: 'page-rango-ingresos',
  templateUrl: './rango-ingresos.component.html'
})
export class RangoIngresosComponent extends RangoIngresosGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
