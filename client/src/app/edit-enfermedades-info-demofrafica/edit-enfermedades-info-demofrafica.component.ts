import { Component, Injector } from '@angular/core';
import { EditEnfermedadesInfoDemofraficaGenerated } from './edit-enfermedades-info-demofrafica-generated.component';

@Component({
  selector: 'page-edit-enfermedades-info-demofrafica',
  templateUrl: './edit-enfermedades-info-demofrafica.component.html'
})
export class EditEnfermedadesInfoDemofraficaComponent extends EditEnfermedadesInfoDemofraficaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
