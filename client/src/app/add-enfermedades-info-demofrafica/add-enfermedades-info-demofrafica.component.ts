import { Component, Injector } from '@angular/core';
import { AddEnfermedadesInfoDemofraficaGenerated } from './add-enfermedades-info-demofrafica-generated.component';

@Component({
  selector: 'page-add-enfermedades-info-demofrafica',
  templateUrl: './add-enfermedades-info-demofrafica.component.html'
})
export class AddEnfermedadesInfoDemofraficaComponent extends AddEnfermedadesInfoDemofraficaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
