using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Actividades_Bienestar", Schema = "public")]
  public partial class ActividadesBienestar
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("ActividadesBienestar")]
    public ICollection<ActividadesPersona> ActividadesPersonas { get; set; }
  }
}
