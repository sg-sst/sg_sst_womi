import { Component, Injector } from '@angular/core';
import { AddEstadoEmpresaGenerated } from './add-estado-empresa-generated.component';

@Component({
  selector: 'page-add-estado-empresa',
  templateUrl: './add-estado-empresa.component.html'
})
export class AddEstadoEmpresaComponent extends AddEstadoEmpresaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
