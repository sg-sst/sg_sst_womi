import { Component, Injector } from '@angular/core';
import { EstadoCivilsGenerated } from './estado-civils-generated.component';

@Component({
  selector: 'page-estado-civils',
  templateUrl: './estado-civils.component.html'
})
export class EstadoCivilsComponent extends EstadoCivilsGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
