using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Tiempo_Libre", Schema = "public")]
  public partial class TiempoLibre
  {
    [ConcurrencyCheck]
    public string Nombre
    {
      get;
      set;
    }
    [Key]
    public Int64 Id
    {
      get;
      set;
    }


    [InverseProperty("TiempoLibre")]
    public ICollection<InfoDemografica> InfoDemograficas { get; set; }
  }
}
