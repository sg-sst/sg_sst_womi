using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNet.OData.Query;



namespace Sgsst.Controllers.Sgsst
{
  using Models;
  using Data;
  using Models.Sgsst;

  [ODataRoutePrefix("odata/sgsst/EnfermedadesInfoDemofraficas")]
  [Route("mvc/odata/sgsst/EnfermedadesInfoDemofraficas")]
  public partial class EnfermedadesInfoDemofraficasController : ODataController
  {
    private Data.SgsstContext context;

    public EnfermedadesInfoDemofraficasController(Data.SgsstContext context)
    {
      this.context = context;
    }
    // GET /odata/Sgsst/EnfermedadesInfoDemofraficas
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Models.Sgsst.EnfermedadesInfoDemofrafica> GetEnfermedadesInfoDemofraficas()
    {
      var items = this.context.EnfermedadesInfoDemofraficas.AsQueryable<Models.Sgsst.EnfermedadesInfoDemofrafica>();
      this.OnEnfermedadesInfoDemofraficasRead(ref items);

      return items;
    }

    partial void OnEnfermedadesInfoDemofraficasRead(ref IQueryable<Models.Sgsst.EnfermedadesInfoDemofrafica> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Enfermedad_Id},{Info_Demografica_Id}")]
    public SingleResult<EnfermedadesInfoDemofrafica> GetEnfermedadesInfoDemofrafica([FromODataUri] Int64 keyEnfermedad_Id,[FromODataUri] Int64 keyInfo_Demografica_Id)
    {
        var items = this.context.EnfermedadesInfoDemofraficas.Where(i=>i.Enfermedad_Id == keyEnfermedad_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);
        return SingleResult.Create(items);
    }
    partial void OnEnfermedadesInfoDemofraficaDeleted(Models.Sgsst.EnfermedadesInfoDemofrafica item);

    [HttpDelete("{Enfermedad_Id},{Info_Demografica_Id}")]
    public IActionResult DeleteEnfermedadesInfoDemofrafica([FromODataUri] Int64 keyEnfermedad_Id,[FromODataUri] Int64 keyInfo_Demografica_Id)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var items = this.context.EnfermedadesInfoDemofraficas
                .Where(i => i.Enfermedad_Id == keyEnfermedad_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.EnfermedadesInfoDemofrafica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEnfermedadesInfoDemofraficaDeleted(item);
            this.context.EnfermedadesInfoDemofraficas.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEnfermedadesInfoDemofraficaUpdated(Models.Sgsst.EnfermedadesInfoDemofrafica item);

    [HttpPut("{Enfermedad_Id},{Info_Demografica_Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutEnfermedadesInfoDemofrafica([FromODataUri] Int64 keyEnfermedad_Id,[FromODataUri] Int64 keyInfo_Demografica_Id, [FromBody]Models.Sgsst.EnfermedadesInfoDemofrafica newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.EnfermedadesInfoDemofraficas
                .Where(i => i.Enfermedad_Id == keyEnfermedad_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id)
                .AsQueryable();

            items = EntityPatch.ApplyTo<Models.Sgsst.EnfermedadesInfoDemofrafica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEnfermedadesInfoDemofraficaUpdated(newItem);
            this.context.EnfermedadesInfoDemofraficas.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.EnfermedadesInfoDemofraficas.Where(i => i.Enfermedad_Id == keyEnfermedad_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);
            Request.QueryString = Request.QueryString.Add("$expand", "Enfermedade,InfoDemografica");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Enfermedad_Id},{Info_Demografica_Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchEnfermedadesInfoDemofrafica([FromODataUri] Int64 keyEnfermedad_Id,[FromODataUri] Int64 keyInfo_Demografica_Id, [FromBody]Delta<Models.Sgsst.EnfermedadesInfoDemofrafica> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.EnfermedadesInfoDemofraficas.Where(i => i.Enfermedad_Id == keyEnfermedad_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);

            items = EntityPatch.ApplyTo<Models.Sgsst.EnfermedadesInfoDemofrafica>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnEnfermedadesInfoDemofraficaUpdated(item);
            this.context.EnfermedadesInfoDemofraficas.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.EnfermedadesInfoDemofraficas.Where(i => i.Enfermedad_Id == keyEnfermedad_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);
            Request.QueryString = Request.QueryString.Add("$expand", "Enfermedade,InfoDemografica");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnEnfermedadesInfoDemofraficaCreated(Models.Sgsst.EnfermedadesInfoDemofrafica item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Models.Sgsst.EnfermedadesInfoDemofrafica item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnEnfermedadesInfoDemofraficaCreated(item);
            this.context.EnfermedadesInfoDemofraficas.Add(item);
            this.context.SaveChanges();

            var keyEnfermedad_Id = item.Enfermedad_Id;
            var keyInfo_Demografica_Id = item.Info_Demografica_Id;

            var itemToReturn = this.context.EnfermedadesInfoDemofraficas.Where(i => i.Enfermedad_Id == keyEnfermedad_Id && i.Info_Demografica_Id == keyInfo_Demografica_Id);

            Request.QueryString = Request.QueryString.Add("$expand", "Enfermedade,InfoDemografica");

            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
