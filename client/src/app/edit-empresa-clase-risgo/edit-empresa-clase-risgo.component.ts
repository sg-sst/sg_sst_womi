import { Component, Injector } from '@angular/core';
import { EditEmpresaClaseRisgoGenerated } from './edit-empresa-clase-risgo-generated.component';

@Component({
  selector: 'page-edit-empresa-clase-risgo',
  templateUrl: './edit-empresa-clase-risgo.component.html'
})
export class EditEmpresaClaseRisgoComponent extends EditEmpresaClaseRisgoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
