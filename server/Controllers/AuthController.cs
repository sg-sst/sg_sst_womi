using System;
using System.Linq;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

using Sgsst.Authentication;
using Sgsst.Data;
using Sgsst.Models;

namespace Sgsst.Controllers
{
    [Route("/auth/[action]")]
    public partial class AuthController : Controller
    {
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IHostingEnvironment env;
        public AuthController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IHostingEnvironment env)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.env = env;
        }

        private IActionResult Error(string message)
        {
            return BadRequest(new { error = new { message } });
        }

        private IActionResult Jwt(IEnumerable<Claim> claims)
        {
            var handler = new JwtSecurityTokenHandler();

            var token = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = TokenProviderOptions.Issuer,
                Audience = TokenProviderOptions.Audience,
                SigningCredentials = TokenProviderOptions.SigningCredentials,
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.Add(TokenProviderOptions.Expiration)
            });

            return Json(new { access_token = handler.WriteToken(token), expires_in = TokenProviderOptions.Expiration.TotalSeconds });
        }

        partial void OnChangePassword(ApplicationUser user, string newPassword);

        [Authorize(AuthenticationSchemes="Bearer")]
        [HttpPost("/auth/change-password")]
        public async Task<IActionResult> ChangePassword([FromBody]JObject data)
        {
            var oldPassword = data.GetValue("OldPassword", StringComparison.OrdinalIgnoreCase);
            var newPassword = data.GetValue("NewPassword", StringComparison.OrdinalIgnoreCase);

            if (oldPassword == null || newPassword == null)
            {
                return Error("Invalid old or new password.");
            }

            var id = this.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var user = await userManager.FindByIdAsync(id);

            OnChangePassword(user, newPassword.ToObject<string>());

            var result = await userManager.ChangePasswordAsync(user, oldPassword.ToObject<string>(), newPassword.ToObject<string>());

            if (!result.Succeeded)
            {
                var message = string.Join(", ", result.Errors.Select(error => error.Description));

                return Error(message);
            }

            return new NoContentResult();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]JObject data)
        {
            var username = data.GetValue("UserName", StringComparison.OrdinalIgnoreCase);
            var password = data.GetValue("Password", StringComparison.OrdinalIgnoreCase);

            if (username == null || password == null)
            {
                return Error("Invalid user name or password.");
            }

            if (env.EnvironmentName == "Development" && username.ToObject<string>() == "admin" && password.ToObject<string>() == "admin")
            {
                var claims = new List<Claim>() {
                        new Claim(ClaimTypes.Name, "admin"),
                        new Claim(ClaimTypes.Email, "admin")
                      };

                this.roleManager.Roles.ToList().ForEach(r => claims.Add(new Claim(ClaimTypes.Role, r.Name)));

                return Jwt(claims);
            }

            var user = await userManager.FindByNameAsync(username.ToObject<string>());

            if (user == null)
            {
                return Error("Invalid user name or password.");
            }

            var validPassword = await userManager.CheckPasswordAsync(user, password.ToObject<string>());

            if (!validPassword)
            {
                return Error("Invalid user name or password.");
            }
            if (!user.EmailConfirmed)
            {
                return Error("User email not confirmed.");
            }
            var principal = await signInManager.CreateUserPrincipalAsync(user);

            return Jwt(principal.Claims);
        }

        partial void OnUserCreated(ApplicationUser user);

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]JObject data)
        {
            var email = data.GetValue("Email", StringComparison.OrdinalIgnoreCase);
            var password = data.GetValue("Password", StringComparison.OrdinalIgnoreCase);

            if (email == null || password == null)
            {
                return Error("Invalid email or password.");
            }

            var user = new ApplicationUser { UserName = email.ToObject<string>(), Email = email.ToObject<string>() };

            EntityPatch.Apply(user, data);

            OnUserCreated(user);

            var result = await userManager.CreateAsync(user, password.ToObject<string>());

            if (result.Succeeded)
            {
                await SendConfirmationEmail(userManager, user, Url, Request.Scheme);
                return Created($"auth/Users('{user.Id}')", user);
            }
            else
            {
                return IdentityError(result);
            }
        }

        private IActionResult IdentityError(IdentityResult result)
        {
            var message = string.Join(", ", result.Errors.Select(error => error.Description));

            return BadRequest(new { error = new { message } });
        }

        public static async Task SendConfirmationEmail(UserManager<ApplicationUser> userManager, ApplicationUser user, IUrlHelper url, string scheme)
        {
            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = url.Action("ConfirmEmail", "Auth", new { userId = user.Id, code = code }, protocol: scheme);

            await SendEmail(user, code, callbackUrl, "Por favor confirme su direccion de correo electronica", "Por favor confirme su direccion de correo electronica");
        }

        static partial void OnSendEmail(System.Net.Mail.MailMessage mailMessage);

        public static async Task SendEmail(ApplicationUser user, string code, string callbackUrl, string subject, string text)
        {
            var client = new System.Net.Mail.SmtpClient("smtp.gmail.com");
            client.UseDefaultCredentials = false;

            client.EnableSsl = true;

            client.Credentials = new System.Net.NetworkCredential("enuarmunozcastillo@gmail.com", "1082778631Enuar");

            var mailMessage = new System.Net.Mail.MailMessage();
            mailMessage.From = new System.Net.Mail.MailAddress("enuarmunozcastillo@gmail.com");
            mailMessage.To.Add(user.Email);
            if(callbackUrl != null)
            {
                mailMessage.Body = string.Format(@"<a href=""{0}"">{1}</a>", callbackUrl, text);
            } else
            {
                mailMessage.Body = text;
            }

            mailMessage.Subject = subject;
            mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
            mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
            mailMessage.IsBodyHtml = true;

            OnSendEmail(mailMessage);

            await client.SendMailAsync(mailMessage);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            var user = await userManager.FindByIdAsync(userId);
            var result = await userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                user.EmailConfirmed = true;
                                return Redirect("https://localhost:8000");
            }

            return new NoContentResult();
        }

        [HttpPost("/auth/reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody]JObject data)
        {
            var username = data.GetValue("UserName", StringComparison.OrdinalIgnoreCase);

            if (username == null)
            {
                return Error("Invalid user name.");
            }

            var user = await userManager.FindByNameAsync(username.ToObject<string>());

            if (user == null)
            {
                return Error("Invalid user.");
            }

            if (!user.EmailConfirmed)
            {
                return Error("User email not confirmed.");
            }

            await SendResetPasswordEmail(userManager, user, Url, Request.Scheme);

            return new NoContentResult();
        }

        public static async Task SendResetPasswordEmail(UserManager<ApplicationUser> userManager, ApplicationUser user, IUrlHelper url, string scheme)
        {
            var code = await userManager.GeneratePasswordResetTokenAsync(user);
            var callbackUrl = url.Action("ConfirmResetPassword", "Auth", new { userId = user.Id, code = code }, protocol: scheme);

            await SendEmail(user, code, callbackUrl, "Confirm password reset", "Confirm password reset");
        }

        partial void OnConfirmResetPassword(string userId, string code, string newPassword);

        [AllowAnonymous]
        public async Task<IActionResult> ConfirmResetPassword(string userId, string code)
        {
            var user = await userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return Error("Invalid user.");
            }

            var newPassword = GenerateRandomPassword();

            OnConfirmResetPassword(userId, code, newPassword);

            var result = await userManager.ResetPasswordAsync(user, code, newPassword);
            if (result.Succeeded)
            {
                await SendEmail(user, code, null, "New password", newPassword);
                                return Redirect("https://localhost:8000");
            }

            return new NoContentResult();
        }

        public static string GenerateRandomPassword(PasswordOptions options = null)
        {
            if (options == null)
            {
                options = new PasswordOptions()
                {
                    RequiredLength = 8,
                    RequiredUniqueChars = 4,
                    RequireDigit = true,
                    RequireLowercase = true,
                    RequireNonAlphanumeric = true,
                    RequireUppercase = true
                };
            }

            var randomChars = new[] {
                    "ABCDEFGHJKLMNOPQRSTUVWXYZ",
                    "abcdefghijkmnopqrstuvwxyz",
                    "0123456789",
                    "!@$?_-"
                };

            var rand = new Random(Environment.TickCount);
            var chars = new List<char>();

            if (options.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (options.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (options.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (options.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < options.RequiredLength
                || chars.Distinct().Count() < options.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }
    }
}
