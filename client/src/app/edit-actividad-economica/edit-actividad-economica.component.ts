import { Component, Injector } from '@angular/core';
import { EditActividadEconomicaGenerated } from './edit-actividad-economica-generated.component';

@Component({
  selector: 'page-edit-actividad-economica',
  templateUrl: './edit-actividad-economica.component.html'
})
export class EditActividadEconomicaComponent extends EditActividadEconomicaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
