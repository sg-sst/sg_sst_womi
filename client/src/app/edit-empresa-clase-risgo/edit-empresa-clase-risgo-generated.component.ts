/*
  This file is automatically generated. Any changes will be overwritten.
  Modify edit-empresa-clase-risgo.component.ts instead.
*/
import { LOCALE_ID, ChangeDetectorRef, ViewChild, AfterViewInit, Injector, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';

import { DialogService, DIALOG_PARAMETERS, DialogRef } from '@radzen/angular/dist/dialog';
import { NotificationService } from '@radzen/angular/dist/notification';
import { ContentComponent } from '@radzen/angular/dist/content';
import { LabelComponent } from '@radzen/angular/dist/label';
import { ButtonComponent } from '@radzen/angular/dist/button';
import { FormComponent } from '@radzen/angular/dist/form';

import { ConfigService } from '../config.service';

import { SgsstService } from '../sgsst.service';
import { SecurityService } from '../security.service';

export class EditEmpresaClaseRisgoGenerated implements AfterViewInit, OnInit, OnDestroy {
  // Components
  @ViewChild('content1') content1: ContentComponent;
  @ViewChild('closeLabel') closeLabel: LabelComponent;
  @ViewChild('closeButton') closeButton: ButtonComponent;
  @ViewChild('label0') label0: LabelComponent;
  @ViewChild('button0') button0: ButtonComponent;
  @ViewChild('form0') form0: FormComponent;

  router: Router;

  cd: ChangeDetectorRef;

  route: ActivatedRoute;

  notificationService: NotificationService;

  configService: ConfigService;

  dialogService: DialogService;

  dialogRef: DialogRef;

  httpClient: HttpClient;

  locale: string;

  _location: Location;

  _subscription: Subscription;

  sgsst: SgsstService;

  security: SecurityService;
  hasChanges: any;
  canEdit: any;
  empresaclaserisgo: any;
  getByEmpresasForEmpresa_IdResult: any;
  getByClaseRiesgosForClase_Riesgo_IdResult: any;
  getEmpresasForEmpresa_IdPageSize: any;
  getEmpresasForEmpresa_IdResult: any;
  getEmpresasForEmpresa_IdCount: any;
  getClaseRiesgosForClase_Riesgo_IdPageSize: any;
  getClaseRiesgosForClase_Riesgo_IdResult: any;
  getClaseRiesgosForClase_Riesgo_IdCount: any;
  parameters: any;

  constructor(private injector: Injector) {
  }

  ngOnInit() {
    this.notificationService = this.injector.get(NotificationService);

    this.configService = this.injector.get(ConfigService);

    this.dialogService = this.injector.get(DialogService);

    this.dialogRef = this.injector.get(DialogRef, null);

    this.locale = this.injector.get(LOCALE_ID);

    this.router = this.injector.get(Router);

    this.cd = this.injector.get(ChangeDetectorRef);

    this._location = this.injector.get(Location);

    this.route = this.injector.get(ActivatedRoute);

    this.httpClient = this.injector.get(HttpClient);

    this.sgsst = this.injector.get(SgsstService);
    this.security = this.injector.get(SecurityService);
  }

  ngAfterViewInit() {
    this._subscription = this.route.params.subscribe(parameters => {
      if (this.dialogRef) {
        this.parameters = this.injector.get(DIALOG_PARAMETERS);
      } else {
        this.parameters = parameters;
      }
      this.load();
      this.cd.detectChanges();
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }


  load() {
    this.hasChanges = false;

    this.canEdit = true;

    this.sgsst.getEmpresaClaseRisgoByEmpresaIdAndClaseRiesgoId(this.parameters.Empresa_Id, this.parameters.Clase_Riesgo_Id, null)
    .subscribe((result: any) => {
      this.empresaclaserisgo = result;

      if (this.empresaclaserisgo.Empresa_Id != null) {
              this.sgsst.getEmpresaById(null, this.empresaclaserisgo.Empresa_Id)
        .subscribe((result: any) => {
              this.getByEmpresasForEmpresa_IdResult = result;
        }, (result: any) => {
      
        });
      }

      if (this.empresaclaserisgo.Clase_Riesgo_Id != null) {
              this.sgsst.getClaseRiesgoById(null, this.empresaclaserisgo.Clase_Riesgo_Id)
        .subscribe((result: any) => {
              this.getByClaseRiesgosForClase_Riesgo_IdResult = result;
        }, (result: any) => {
      
        });
      }
    }, (result: any) => {
      this.canEdit = !(result.status == 400);
    });

    this.getEmpresasForEmpresa_IdPageSize = 10;

    this.sgsst.getEmpresas(null, this.getEmpresasForEmpresa_IdPageSize, 0, null, true, null, null, null)
    .subscribe((result: any) => {
      this.getEmpresasForEmpresa_IdResult = result.value;

      this.getEmpresasForEmpresa_IdCount = result['@odata.count'];
    }, (result: any) => {

    });

    this.getClaseRiesgosForClase_Riesgo_IdPageSize = 10;

    this.sgsst.getClaseRiesgos(null, this.getClaseRiesgosForClase_Riesgo_IdPageSize, 0, null, true, null, null, null)
    .subscribe((result: any) => {
      this.getClaseRiesgosForClase_Riesgo_IdResult = result.value;

      this.getClaseRiesgosForClase_Riesgo_IdCount = result['@odata.count'];
    }, (result: any) => {

    });
  }

  closeButtonClick(event: any) {
    if (this.dialogRef) {
      this.dialogRef.close();
    } else {
      this._location.back();
    }
  }

  button0Click(event: any) {
    this.load()
  }

  form0Cancel(event: any) {
    if (this.dialogRef) {
      this.dialogRef.close();
    } else {
      this._location.back();
    }
  }

  form0Submit(event: any) {
    this.sgsst.updateEmpresaClaseRisgo(this.parameters.Empresa_Id, this.parameters.Clase_Riesgo_Id, null, event)
    .subscribe((result: any) => {
      if (this.dialogRef) {
        this.dialogRef.close();
      } else {
        this._location.back();
      }
    }, (result: any) => {
      this.hasChanges = result.status == 412;

      this.canEdit = !(result.status == 400);

      this.notificationService.notify({ severity: "error", summary: `Error`, detail: `Unable to update EmpresaClaseRisgo` });
    });
  }

  form0LoadData(event: any) {
    if (event.property == 'Empresa_Id') {
          this.sgsst.getEmpresas(`${event.filter}`, event.top, event.skip, `${event.orderby}`, true, null, null, null)
      .subscribe((result: any) => {
          this.getEmpresasForEmpresa_IdResult = result.value;

      this.getEmpresasForEmpresa_IdCount = result['@odata.count'];
      }, (result: any) => {
    
      });
    }

    if (event.property == 'Clase_Riesgo_Id') {
          this.sgsst.getClaseRiesgos(`${event.filter}`, event.top, event.skip, `${event.orderby}`, true, null, null, null)
      .subscribe((result: any) => {
          this.getClaseRiesgosForClase_Riesgo_IdResult = result.value;

      this.getClaseRiesgosForClase_Riesgo_IdCount = result['@odata.count'];
      }, (result: any) => {
    
      });
    }
  }
}
