using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sgsst.Models.Sgsst
{
  [Table("Enfermedades_Info_Demofrafica", Schema = "public")]
  public partial class EnfermedadesInfoDemofrafica
  {
    [Key]
    public Int64 Enfermedad_Id
    {
      get;
      set;
    }

    [ForeignKey("Enfermedad_Id")]
    public Enfermedade Enfermedade { get; set; }
    public Int64 Info_Demografica_Id
    {
      get;
      set;
    }

    [ForeignKey("Info_Demografica_Id")]
    public InfoDemografica InfoDemografica { get; set; }
  }
}
