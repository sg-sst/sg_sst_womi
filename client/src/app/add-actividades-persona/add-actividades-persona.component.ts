import { Component, Injector } from '@angular/core';
import { AddActividadesPersonaGenerated } from './add-actividades-persona-generated.component';

@Component({
  selector: 'page-add-actividades-persona',
  templateUrl: './add-actividades-persona.component.html'
})
export class AddActividadesPersonaComponent extends AddActividadesPersonaGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
