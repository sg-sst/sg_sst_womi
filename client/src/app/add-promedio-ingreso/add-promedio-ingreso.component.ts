import { Component, Injector } from '@angular/core';
import { AddPromedioIngresoGenerated } from './add-promedio-ingreso-generated.component';

@Component({
  selector: 'page-add-promedio-ingreso',
  templateUrl: './add-promedio-ingreso.component.html'
})
export class AddPromedioIngresoComponent extends AddPromedioIngresoGenerated {
  constructor(injector: Injector) {
    super(injector);
  }
}
